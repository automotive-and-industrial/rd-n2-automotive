/*
 * Copyright (c) 2022, Arm Limited.
 * SPDX-License-Identifier: GPL-2.0-only
 *
 * Arm R64 Remote Processor driver
 *
 * Yanqin Wei <Yanqin.Wei@arm.com>
 * Jingzhao Ni <Jingzhao.Ni@arm.com>
 *
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/irqreturn.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of_reserved_mem.h>
#include <linux/platform_device.h>
#include <linux/remoteproc.h>
#include <linux/mailbox_client.h>
#include <linux/dma-mapping.h>

/* This is defined in drivers/remoteproc/remoteproc_internal.h in the kernel
   source */
irqreturn_t rproc_vq_interrupt(struct rproc *rproc, int vq_id);

#define MAX_VQ_NUM 2
#define RX_MAILBOX_START_ID 2

struct armv8r64_rproc_mbox {
	struct mbox_chan *chan;
	struct mbox_client client;
	struct work_struct vq_work;
	int notifyid;
};

struct armv8r64_rproc {
	struct armv8r64_rproc_mbox mb_rx[MAX_VQ_NUM];
	struct armv8r64_rproc_mbox mb_tx[MAX_VQ_NUM];
	struct workqueue_struct *workqueue;
	struct rproc *rproc;
};

static void armv8r64_rproc_kick(struct rproc *rproc, int vqid)
{
	struct armv8r64_rproc *r64proc = rproc->priv;
	struct device *dev = rproc->dev.parent;
	static const char empty_msg = 0;
	int ret;

	dev_dbg(dev, "kick: virtqueue %d\n", vqid);

	if (vqid >= MAX_VQ_NUM) {
		dev_err(dev, "Invalid vqid %d.\n", vqid);
		return;
	}

	ret = mbox_send_message(r64proc->mb_tx[vqid].chan, &empty_msg);
	if (ret < 0)
		dev_dbg(dev, "failed to send mailbox message, status = %d\n", ret);
}

static int armv8r64_rproc_attach(struct rproc *rproc)
{
/* It can be attached without doing anything. */
	return 0;
}

static struct rproc_ops armv8r64_rproc_ops = {
	.kick   = armv8r64_rproc_kick,
	.attach = armv8r64_rproc_attach,
};

static int armv8r64_rproc_mem_alloc(struct rproc *rproc,
				    struct rproc_mem_entry *mem)
{
	struct device *dev = &rproc->dev;
	void *va;

	dev_dbg(dev, "map memory: %pa+%zx\n", &mem->dma, mem->len);
	va = ioremap_wc(mem->dma, mem->len);
	if (!va) {
		dev_err(dev, "Unable to map memory region: %pa+%zx\n",
			&mem->dma, mem->len);
		return -ENOMEM;
	}

	mem->va = va;

	return 0;
}

static int armv8r64_rproc_mem_release(struct rproc *rproc,
				      struct rproc_mem_entry *mem)
{
	dev_dbg(&rproc->dev, "unmap memory: %pa\n", &mem->dma);
	iounmap(mem->va);

	return 0;
}

static int armv8r64_rproc_parse_dt(struct platform_device * pdev,
				 struct rproc *rproc)
{
	struct device *dev = rproc->dev.parent;
	struct device_node *np = dev->of_node;
	u64 phy_base_addr;
	u64 dev_base_addr;
	u32 reg_name_count;
	struct resource *reg_res;
	void __iomem *resource_table_va;
	struct rproc_mem_entry *mem;
	u32 da;
	int ret = 0;
	int i;

	ret = of_property_read_u64(np, "dev-base-addr", &dev_base_addr);
	if (ret) {
		dev_err(dev, "failed to read dev-base-addr in '%pOF'\n", np);
		return ret;
	}

	reg_res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (reg_res) {
		phy_base_addr = reg_res->start;
	} else {
		dev_err(dev, "failed to get reg base address.\n", 0);
		return -EINVAL;
	}

	ret = of_property_read_u32(np, "#reg-name-cells", &reg_name_count);
	if (ret) {
		dev_err(dev, "failed to read #reg-name-cells in '%pOF'\n", np);
		return ret;
	}

	for (i = 0; i < reg_name_count; i++) {
		const char *reg_name;

		ret = of_property_read_string_index(np, "reg-name", i, &reg_name);
		if (ret) {
			dev_err(dev, "failed to read reg-name %d in '%pOF'\n", i, np);
			return ret;
		}

		reg_res = platform_get_resource(pdev, IORESOURCE_MEM, i);
		if (!reg_res) {
			dev_err(dev, "failed to get reg %d address.\n", i);
			return -EINVAL;
		}

		if (!strcmp(reg_name, "resource_table")) {
			resource_table_va = devm_ioremap_wc(dev, reg_res->start,
							resource_size(reg_res));

			if (IS_ERR_OR_NULL(resource_table_va)) {
				dev_err(dev, "Unable to map memory region: %pa to %pa\n",
					&reg_res->start, &reg_res->end);
				return -ENOMEM;
			}

			rproc->cached_table = NULL;
			rproc->table_sz = resource_size(reg_res);
			rproc->table_ptr = (struct resource_table *)resource_table_va;
		} else {
			/* Register memory region */
			da = reg_res->start - phy_base_addr + dev_base_addr;
			mem = rproc_mem_entry_init(dev, NULL,
						   reg_res->start,
						   resource_size(reg_res), da,
						   armv8r64_rproc_mem_alloc,
						   armv8r64_rproc_mem_release,
						   reg_name);
			if (!mem)
				return -ENOMEM;

			rproc_add_carveout(rproc, mem);
		}
	}

	return 0;
}

static void armv8r64_rproc_mb_vq_work(struct work_struct *work)
{
	struct armv8r64_rproc_mbox *mb;
	struct rproc *rproc;

	mb = container_of(work, struct armv8r64_rproc_mbox, vq_work);
	rproc = dev_get_drvdata(mb->client.dev);

	dev_dbg(mb->client.dev, "mbox callback: %d\n", mb->notifyid);

	if (rproc_vq_interrupt(rproc, mb->notifyid) == IRQ_NONE)
		dev_dbg(&rproc->dev, "no message found in vq%d\n", mb->notifyid);
}

static void armv8r64_rproc_mbox_callback(struct mbox_client *client, void *data)
{
	struct rproc *rproc = dev_get_drvdata(client->dev);
	struct armv8r64_rproc_mbox *mb;
	struct armv8r64_rproc *r64rproc;

	r64rproc = rproc->priv;
	mb = container_of(client, struct armv8r64_rproc_mbox, client);

	queue_work(r64rproc->workqueue, &mb->vq_work);
}

static int armv8r64_rproc_request_mbox(struct rproc *rproc)
{
	int ret;
	int vq_id;
	struct armv8r64_rproc *r64rproc = rproc->priv;
	struct device *dev = &rproc->dev;
	struct armv8r64_rproc_mbox *mb;

	for (vq_id = 0; vq_id < MAX_VQ_NUM; vq_id++) {
		mb = &r64rproc->mb_tx[vq_id];
		mb->notifyid = vq_id;
		mb->client.dev = dev->parent;
		mb->client.tx_done = NULL;
		mb->client.tx_block = false;
		mb->chan = mbox_request_channel(&mb->client, vq_id);

		if (IS_ERR(mb->chan)) {
			ret = -EBUSY;
			dev_err(dev, "mbox_request_channel tx %d failed: %ld\n",
				vq_id, PTR_ERR(mb->chan));
			r64rproc->mb_tx[vq_id].chan = NULL;
			return ret;
		}
	}

	for (vq_id = 0; vq_id < MAX_VQ_NUM; vq_id++) {
		mb = &r64rproc->mb_rx[vq_id];
		mb->notifyid = vq_id;
		mb->client.dev = dev->parent;
		mb->client.rx_callback = armv8r64_rproc_mbox_callback;
		mb->chan = mbox_request_channel(&mb->client, vq_id + RX_MAILBOX_START_ID);

		if (IS_ERR(mb->chan)) {
			ret = -EBUSY;
			dev_err(dev, "mbox_request_channel rx %d failed: %ld\n",
				vq_id, PTR_ERR(mb->chan));
			r64rproc->mb_rx[vq_id].chan = NULL;
			return ret;
		}

		INIT_WORK(&r64rproc->mb_rx[vq_id].vq_work, armv8r64_rproc_mb_vq_work);
	}

	r64rproc->workqueue = create_workqueue(dev_name(dev));
	if (!r64rproc->workqueue) {
		ret = -ENOMEM;
		dev_err(dev, "cannot create workqueue\n");
		return ret;
	}

	return 0;
}

static void armv8r64_rproc_free_mbox(struct rproc *rproc)
{
	struct armv8r64_rproc *r64rproc = rproc->priv;
	int vq_id;

	for (vq_id = 0; vq_id < MAX_VQ_NUM; vq_id++) {
		if (r64rproc->mb_tx[vq_id].chan)
			mbox_free_channel(r64rproc->mb_tx[vq_id].chan);
		if (r64rproc->mb_rx[vq_id].chan)
			mbox_free_channel(r64rproc->mb_rx[vq_id].chan);
	}

	if (r64rproc->workqueue)
		destroy_workqueue(r64rproc->workqueue);
}

static int armv8r64_rproc_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = dev->of_node;
	struct armv8r64_rproc *r64rproc;
	struct rproc *rproc;
	int ret;

	rproc = rproc_alloc(dev, np->name, &armv8r64_rproc_ops,
			    NULL, sizeof(struct armv8r64_rproc));
	if (!rproc)
		return -ENOMEM;

	r64rproc = rproc->priv;

	memset(r64rproc, 0, sizeof(struct armv8r64_rproc));

	r64rproc->rproc = rproc;
	rproc->has_iommu = false;
	rproc->firmware = NULL;
	rproc->state = RPROC_DETACHED;

	ret = armv8r64_rproc_parse_dt(pdev, rproc);
	if (ret) {
		dev_err(&pdev->dev, "parse memory failed\n");
		return ret;
	}

	ret = armv8r64_rproc_request_mbox(rproc);
	if (ret) {
		dev_err(&pdev->dev, "request mbox and workqueue failed\n");
		armv8r64_rproc_free_mbox(rproc);
		return ret;
	}

	platform_set_drvdata(pdev, rproc);

	ret = rproc_add(rproc);
	if (ret) {
		dev_err(&pdev->dev, "rproc_add failed\n");
		armv8r64_rproc_free_mbox(rproc);
		rproc_free(rproc);
		return ret;
	}

	return 0;
}

static int armv8r64_rproc_remove(struct platform_device *pdev)
{
	struct rproc *rproc = platform_get_drvdata(pdev);

	armv8r64_rproc_free_mbox(rproc);
	rproc_del(rproc);
	rproc_free(rproc);
	return 0;
}

static const struct of_device_id armv8r64_rproc_match[] = {
	{ .compatible = "arm,armv8r64-rproc", },
	{},
};
MODULE_DEVICE_TABLE(of, armv8r64_rproc_match);

static struct platform_driver armv8r64_rproc_driver = {
	.probe = armv8r64_rproc_probe,
	.remove = armv8r64_rproc_remove,
	.driver = {
		.name = "armv8r64-rproc",
		.of_match_table = armv8r64_rproc_match,
	},
};
module_platform_driver(armv8r64_rproc_driver);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Arm R64 Remote Processor control driver");
