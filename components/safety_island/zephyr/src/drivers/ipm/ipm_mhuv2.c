/*
 * Copyright (c) 2022, Arm Limited.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT arm_mhuv2
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "ipm_mhuv2.h"


#define LOG_LEVEL CONFIG_IPM_LOG_LEVEL
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(ipm_mhuv2);

/* DTS operation */
#define MHUV2_PROTO_INFO_ELEM(node_id, prop, idx) \
	(DT_PROP_BY_IDX(node_id, prop, idx)),

static uint32_t ipm_mhuv2_get_channel_idx(struct chan_info *chans, uint32_t num_chans,
			const struct device *d)
{
	uint32_t i = 0, stat;

	while (i < num_chans) {
		stat = sys_read32(
				MHUV2_RECV_CHAN_ST_MASK_ADDR(d, chans[i].chan_regs_idx));
		if (stat) {
			if (chans[i].protocol == DOORBELL)
				i += __builtin_ctz(stat);
			return i;
		}
		i += (chans[i].protocol == DOORBELL) ? IPM_MHUV2_ST_BIT:1;
	}

	return INVALID_CH_INDEX;
}

static int ipm_mhuv2_doorbell_send(const struct device *d,
			uint32_t ch_index, const void *data, int size)
{
	ARG_UNUSED(data);
	ARG_UNUSED(size);

	uint32_t ch_reg_idx, bit;

	ch_reg_idx = MHUV2_DEV_DATA(d)->tx_ch_array[ch_index].chan_regs_idx;
	bit = MHUV2_DEV_DATA(d)->tx_ch_array[ch_index].bit;

	if (!sys_test_bit(MHUV2_SEND_CHAN_ST_ADDR(d, ch_reg_idx), bit)) {
		sys_set_bit(MHUV2_SEND_CHAN_SET_ADDR(d, ch_reg_idx), bit);
		return 0;
	} else {
		return -EBUSY;
	}
}

static void ipm_mhuv2_doorbell_recv(const struct device *d, uint32_t ch_index)
{
	uint32_t ch_reg_idx, bit;

	ch_reg_idx = MHUV2_DEV_DATA(d)->rx_ch_array[ch_index].chan_regs_idx;
	bit = MHUV2_DEV_DATA(d)->rx_ch_array[ch_index].bit;

	sys_set_bit(MHUV2_RECV_CHAN_CLR_ADDR(d, ch_reg_idx), bit);

	if (MHUV2_DEV_DATA(d)->callback != NULL) {
		MHUV2_DEV_DATA(d)->callback(d, MHUV2_DEV_DATA(d)->user_data,
						ch_index, NULL);
	}
}

static int ipm_mhuv2_single_word_send(const struct device *d,
			uint32_t ch_index, const void *data, int size)
{
	ARG_UNUSED(size);
	uint32_t ch_reg_idx, *p_data = (uint32_t *)data;

	ch_reg_idx = MHUV2_DEV_DATA(d)->tx_ch_array[ch_index].chan_regs_idx;

	/* The ch_st reg can not be set to 0 */
	if ((p_data == NULL) || ((*p_data) == 0)) {
		LOG_WRN("Invaild Message data p_data: %p\n", p_data);
		return -EINVAL;
	}

	if (!sys_read32(MHUV2_SEND_CHAN_ST_ADDR(d, ch_reg_idx))) {
		sys_write32((*p_data), MHUV2_SEND_CHAN_SET_ADDR(d, ch_reg_idx));
	} else
		return -EBUSY;
	return 0;
}

static void ipm_mhuv2_single_word_recv(const struct device *d,
	uint32_t ch_index)
{
	uint32_t msg, ch_reg_idx;
	struct ipm_mhuv2_device_data *driver_data = MHUV2_DEV_DATA(d);

	ch_reg_idx = MHUV2_DEV_DATA(d)->rx_ch_array[ch_index].chan_regs_idx;

	msg = sys_read32(MHUV2_RECV_CHAN_ST_ADDR(d, ch_reg_idx));
	sys_write32(0xFFFFFFFF, MHUV2_RECV_CHAN_CLR_ADDR(d, ch_reg_idx));

	if ((driver_data != NULL) && (driver_data->callback != NULL)) {
		driver_data->callback(d, driver_data->user_data, ch_index, &msg);
	} else {
		LOG_WRN("IPM mhuv2 recv callback failed. driver_data %p, cb %p\n",
			driver_data, driver_data->callback);
	}
}

static int ipm_mhuv2_chan_init(struct chan_info *ch_array,
	const uint32_t *dt_ch_conf, int length)
{
	int ch_index = 0;
	uint32_t width, proto, ch_reg_idx = 0;

	for (uint32_t i = 0; i < length; i += 2) {
		proto = dt_ch_conf[i];
		width = dt_ch_conf[i+1];
		if (proto == SINGLE_WORD) {
			if (width == 1) {
				ch_array[ch_index].chan_regs_idx = ch_reg_idx;
				ch_array[ch_index].recv = ipm_mhuv2_single_word_recv;
				ch_array[ch_index].send = ipm_mhuv2_single_word_send;
				ch_array[ch_index++].protocol = proto;
				ch_reg_idx++;
			} else {
				LOG_ERR("Multi-word transfer model does not support\n");
				return -EINVAL;
			}
		} else if (proto == DOORBELL) {
			for (uint32_t j = 0; j < width; j++) {
				for (uint32_t k = 0; k < IPM_MHUV2_ST_BIT; k++) {
					if (ch_index >= CONFIG_IPM_MHUV2_MAX_LOGICAL_CH_NUM) {
						LOG_ERR("Chan idx:%u is larger than max size:%u\n",
							ch_index++,
							CONFIG_IPM_MHUV2_MAX_LOGICAL_CH_NUM);
						return -EINVAL;
					}
					ch_array[ch_index].chan_regs_idx = ch_reg_idx;
					ch_array[ch_index].bit = k;
					ch_array[ch_index].send = ipm_mhuv2_doorbell_send;
					ch_array[ch_index].recv = ipm_mhuv2_doorbell_recv;
					ch_array[ch_index++].protocol = proto;
				}
				ch_reg_idx++;
			}
		} else
			LOG_ERR("Unknown transfer protocol: %u\n", proto);
	}
	return ch_index;
}

static int ipm_mhuv2_rx_init(const struct device *dev)
{
	uint32_t mhu_cfg = sys_read32(MHUV2_RECV_CFG_ADDR(dev));
	uint32_t window = MHUV2_RECV_CFG_NUM_CH_REG(mhu_cfg);
	int num_chans;

	/* Disable all the channel windows */
	for (int i = 0; i < window; i++)
		sys_write32(0xFFFFFFFF, MHUV2_RECV_CHAN_MSK_SET_ADDR(dev, i));

	num_chans = ipm_mhuv2_chan_init(MHUV2_DEV_DATA(dev)->rx_ch_array,
					MHUV2_DEV_CFG(dev)->rx_chan_conf,
					MHUV2_DEV_CFG(dev)->rx_chan_conf_size);

	if (num_chans < 0) {
		LOG_ERR("Initial Rx Channel failed. error code:%d\n", num_chans);
		return -EINVAL;
	}

	MHUV2_DEV_DATA(dev)->rx_num_ch = num_chans;

	return 0;
}


static int ipm_mhuv2_tx_init(const struct device *dev)
{
	int num_chans;

	num_chans = ipm_mhuv2_chan_init(MHUV2_DEV_DATA(dev)->tx_ch_array,
					MHUV2_DEV_CFG(dev)->tx_chan_conf,
					MHUV2_DEV_CFG(dev)->tx_chan_conf_size);

	if (num_chans < 0) {
		LOG_ERR("Initial Tx Channel failed. error code:%d\n", num_chans);
		return -EINVAL;
	}

	MHUV2_DEV_DATA(dev)->tx_num_ch = num_chans;

	return 0;
}

static int ipm_mhuv2_send(const struct device *d, int wait, uint32_t ch_id,
			  const void *data, int size)
{
	struct chan_info *p_channel = &(MHUV2_DEV_DATA(d)->tx_ch_array[ch_id]);

	return p_channel->send(d, ch_id, data, size);
}

static void ipm_mhuv2_register_cb(const struct device *d,
				ipm_callback_t cb, void *user_data)
{
	struct ipm_mhuv2_device_data *driver_data = MHUV2_DEV_DATA(d);

	driver_data->callback = cb;
	driver_data->user_data = user_data;
}

static int ipm_mhuv2_max_data_size_get(const struct device *d)
{
	ARG_UNUSED(d);
	return IPM_MHUV2_MAX_DATA_SIZE;
}
static uint32_t ipm_mhuv2_max_send_id(const struct device *d)
{
	return MHUV2_DEV_DATA(d)->tx_num_ch;
}

static int ipm_mhuv2_set_enabled(const struct device *d, int enable)
{
	struct chan_info *ch_info = MHUV2_DEV_DATA(d)->rx_ch_array;
	uint32_t i, rx_num_chans = MHUV2_DEV_DATA(d)->rx_num_ch;
	uint32_t mhu_cfg = sys_read32(MHUV2_RECV_CFG_ADDR(d));
	uint32_t window = MHUV2_RECV_CFG_NUM_CH_REG(mhu_cfg);

	if (enable == 0) {
		/* Disable the MHUv2 sender */
		sys_write32(0, MHUV2_SEND_ACC_REQ_ADDR(d));

		/* Disable the MHUv2 receiver */
		for (i = 0; i < window; i++)
			sys_write32(0xFFFFFFFF, MHUV2_RECV_CHAN_MSK_SET_ADDR(d, i));

	} else {
		/* Enable the MHUv2 sender */
		sys_write32(1, MHUV2_SEND_ACC_REQ_ADDR(d));

		/* Read the MHUv2 send ready reg, until ready reg shows success */
		while (!sys_read32(MHUV2_SEND_ACC_RDY_ADDR(d)));

		/* Enable the MHUv2 receiver */
		for (i = 0; i < rx_num_chans; i++) {
			if (ch_info[i].protocol == DOORBELL)
				sys_set_bit(
					MHUV2_RECV_CHAN_MSK_CLR_ADDR(d, ch_info[i].chan_regs_idx),
					ch_info[i].bit);
			else
				sys_write32(0xFFFFFFFF,
					MHUV2_RECV_CHAN_MSK_CLR_ADDR(d, ch_info[i].chan_regs_idx));
		}
	}
	return 0;
}

static void ipm_mhuv2_isr(const struct device *d)
{
	uint32_t num_chans = MHUV2_DEV_DATA(d)->rx_num_ch;
	struct chan_info *p_channel;
	uint32_t ch_index = ipm_mhuv2_get_channel_idx(MHUV2_DEV_DATA(d)->rx_ch_array,
							num_chans, d);

	if (ch_index < num_chans) {
		p_channel = &(MHUV2_DEV_DATA(d)->rx_ch_array[ch_index]);
		if ((p_channel != NULL) && (p_channel->recv != NULL))
			p_channel->recv(d, ch_index);
		else
			LOG_ERR("Ch info ptr is NULL. ch_index: %u\n", ch_index);
	} else
		LOG_WRN("Ch idx %u is greater than the channel number %u\n",
			ch_index, num_chans);
}

static int ipm_mhuv2_init(const struct device *d)
{
	const struct ipm_mhuv2_device_config *config = MHUV2_DEV_CFG(d);
	int ret = 0;

	ret = ipm_mhuv2_tx_init(d);
	if (ret != 0) {
		LOG_ERR("IPM MHUv2 Tx init failed, ret:%d\n", ret);
		return ret;
	}

	ret = ipm_mhuv2_rx_init(d);
	if (ret != 0) {
		LOG_ERR("IPM MHUv2 Rx init failed, ret:%d\n", ret);
		return ret;
	}

	/* Bind Irq */
	if (config->rx_irq_config_func)
		config->rx_irq_config_func(d);
	return ret;
}

static const struct ipm_driver_api ipm_mhuv2_driver_api = {
	.send = ipm_mhuv2_send,
	.register_callback = ipm_mhuv2_register_cb,
	.max_data_size_get = ipm_mhuv2_max_data_size_get,
	.max_id_val_get = ipm_mhuv2_max_send_id,
	.set_enabled = ipm_mhuv2_set_enabled,
};


#define IPM_MHUV2_INIT(n)                                                           \
static void ipm_mhuv2_irq_config_func_rx_##n(const struct device *d);               \
static const struct ipm_mhuv2_device_config ipm_mhuv2_cfg_##n = {                   \
	.rx_base = (uint8_t *)DT_INST_REG_ADDR_BY_NAME(n, rx),                      \
	.tx_base = (uint8_t *)DT_INST_REG_ADDR_BY_NAME(n, tx),                      \
	.rx_irq_config_func = ipm_mhuv2_irq_config_func_rx_##n,                     \
	.tx_chan_conf_size = DT_INST_PROP_LEN(n, tx_chan_conf),                     \
	.tx_chan_conf = {                                                           \
		DT_INST_FOREACH_PROP_ELEM(n, tx_chan_conf, MHUV2_PROTO_INFO_ELEM)   \
	},                                                                          \
	.rx_chan_conf_size = DT_INST_PROP_LEN(n, rx_chan_conf),                     \
	.rx_chan_conf = {                                                           \
		DT_INST_FOREACH_PROP_ELEM(n, rx_chan_conf, MHUV2_PROTO_INFO_ELEM)   \
	}                                                                           \
};                                                                                  \
struct ipm_mhuv2_device_data ipm_mhuv2_data_##n = {                                 \
	.callback = NULL,                                                           \
	.user_data = NULL,                                                          \
	.tx_num_ch = 0,                                                             \
	.rx_num_ch = 0,                                                             \
};                                                                                  \
DEVICE_DT_INST_DEFINE(n,                                                            \
			&ipm_mhuv2_init,                                            \
			NULL,                                                       \
			&ipm_mhuv2_data_##n, &ipm_mhuv2_cfg_##n,                    \
			POST_KERNEL,                                                \
			CONFIG_KERNEL_INIT_PRIORITY_DEVICE,                         \
			&ipm_mhuv2_driver_api);                                     \
static void ipm_mhuv2_irq_config_func_rx_##n(const struct device *d)                \
{                                                                                   \
	IRQ_CONNECT(DT_INST_IRQ_BY_NAME(n, rx, irq),                                \
		DT_INST_IRQ_BY_NAME(n, rx, priority),                               \
			ipm_mhuv2_isr,                                              \
			DEVICE_DT_INST_GET(n),                                      \
			0);                                                         \
	irq_enable(DT_INST_IRQ_BY_NAME(n, rx, irq));                                \
}

DT_INST_FOREACH_STATUS_OKAY(IPM_MHUV2_INIT)
