/*
 * Copyright (c) 2022, Arm Limited.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef IPM_MHUV2_H
#define IPM_MHUV2_H

#include <stdint.h>
#include <zephyr/drivers/ipm.h>
/* Maximum number of channel windows */
#define IPM_MHUV2_MAX_CH_NUM		124
#define IPM_MHUV2_MAX_DATA_SIZE		4
#define IPM_MHUV2_ST_BIT		(IPM_MHUV2_MAX_DATA_SIZE*8)
#define IPM_MHUV2_CFG_NUM_CH_BIT	7


/************************* MHUv2 regs ****************************/
struct ipm_mhuv2_tx_ch {
	uint32_t  ch_st;
	uint8_t   ch_reserved0[8];
	uint32_t  ch_set;
	uint32_t  ch_int_st;
	uint32_t  ch_int_clr;
	uint32_t  ch_int_en;
	uint8_t   ch_reserved1[4];
} __packed;

struct ipm_mhuv2_tx_frame {
	struct ipm_mhuv2_tx_ch chan[IPM_MHUV2_MAX_CH_NUM];
	uint32_t  mhu_cfg;
	uint32_t  resp_cfg;
	uint32_t  access_request;
	uint32_t  access_ready;
	uint32_t  int_st;
	uint32_t  int_clr;
	uint32_t  int_en;
	uint8_t   reserved[100];
} __packed;

struct ipm_mhuv2_rx_ch {
	uint32_t  ch_st;
	uint32_t  ch_st_mask;
	uint32_t  ch_clr;
	uint8_t   ch_reserved0[4];
	uint32_t  ch_msk_st;
	uint32_t  ch_msk_set;
	uint32_t  ch_msk_clr;
	uint8_t   ch_reserved1[4];
} __packed;

struct ipm_mhuv2_rx_frame {
	struct ipm_mhuv2_rx_ch chan[IPM_MHUV2_MAX_CH_NUM];
	uint32_t  mhu_cfg;
	uint8_t   reserved[124];
} __packed;

/* Rx reg operation */
#define MHUV2_RECV_ADDR(dev)	\
		((struct ipm_mhuv2_rx_frame *)((MHUV2_DEV_CFG(dev))->rx_base))

#define MHUV2_RECV_CHAN_ADDR(dev, idx)	\
		((struct ipm_mhuv2_rx_ch *)((MHUV2_RECV_ADDR(dev))->chan + idx))

#define MHUV2_RECV_CHAN_ST_ADDR(dev, idx)	\
		(mm_reg_t)(&(MHUV2_RECV_CHAN_ADDR(dev, idx)->ch_st))

#define MHUV2_RECV_CHAN_ST_MASK_ADDR(dev, idx)	\
		(mm_reg_t)(&(MHUV2_RECV_CHAN_ADDR(dev, idx)->ch_st_mask))

#define MHUV2_RECV_CHAN_CLR_ADDR(dev, idx)	\
		(mm_reg_t)(&(MHUV2_RECV_CHAN_ADDR(dev, idx)->ch_clr))

#define MHUV2_RECV_CHAN_MSK_SET_ADDR(dev, idx)	\
		(mm_reg_t)(&(MHUV2_RECV_CHAN_ADDR(dev, idx)->ch_msk_set))

#define MHUV2_RECV_CHAN_MSK_CLR_ADDR(dev, idx)	\
		(mm_reg_t)(&(MHUV2_RECV_CHAN_ADDR(dev, idx)->ch_msk_clr))

#define MHUV2_RECV_CFG_ADDR(dev)	\
		(mm_reg_t)(&(MHUV2_RECV_ADDR(dev)->mhu_cfg))

#define MHUV2_RECV_CFG_NUM_CH_REG(mhu_cfg)	\
		((mhu_cfg) & ((1 << (IPM_MHUV2_CFG_NUM_CH_BIT)) - 1))


/* Tx reg operation */
#define MHUV2_SEND_ADDR(dev)	\
		((struct ipm_mhuv2_tx_frame *)((MHUV2_DEV_CFG(dev))->tx_base))

#define MHUV2_SEND_CHAN_ADDR(dev, idx)	\
		((struct ipm_mhuv2_tx_ch *)((MHUV2_SEND_ADDR(dev))->chan + idx))

#define MHUV2_SEND_CHAN_ST_ADDR(dev, idx)	\
		(mm_reg_t)(&(MHUV2_SEND_CHAN_ADDR(dev, idx)->ch_st))

#define MHUV2_SEND_CHAN_SET_ADDR(dev, idx)	\
		(mm_reg_t)(&(MHUV2_SEND_CHAN_ADDR(dev, idx)->ch_set))

#define MHUV2_SEND_ACC_REQ_ADDR(dev)	\
		(mm_reg_t)(&((MHUV2_SEND_ADDR(dev))->access_request))

#define MHUV2_SEND_ACC_RDY_ADDR(dev)	\
		(mm_reg_t)(&((MHUV2_SEND_ADDR(dev))->access_ready))


/************************* MHUv2 data structure ****************************/
/* The chan_conf_group_num specifies the maximum group number of the
 * rx/tx_chan_conf in MHUv2's DTS. For each conf group <0 1>, it has 2
 * elements. So, CHAN_CONF_MAX_SIZE equals 2 time of CHAN_CONF_GROUP_NUM.
 * rx/tx_chan_conf[CHAN_CONF_MAX_SIZE] will be used to record this
 * information.
 */
#define IPM_MHUV2_CHAN_CONF_MAX_SIZE	\
	(CONFIG_IPM_MHUV2_CHAN_CONF_GROUP_NUM*2)

enum proto_type {
	DOORBELL = 0,
	SINGLE_WORD,
};

struct chan_info {
	enum proto_type protocol;
	uint32_t chan_regs_idx;
	/* An index which bit will be used by this channel.  */
	/* Only take effect when doorbell is used            */
	uint32_t bit;
	int (*send)(const struct device *d, uint32_t id,
			const void *data, int size);
	void (*recv)(const struct device *d, uint32_t id);
};

struct ipm_mhuv2_device_data {
	ipm_callback_t callback;
	void *user_data;
	/* logic channel info */
	struct chan_info rx_ch_array[CONFIG_IPM_MHUV2_MAX_LOGICAL_CH_NUM];
	struct chan_info tx_ch_array[CONFIG_IPM_MHUV2_MAX_LOGICAL_CH_NUM];
	/* logic channel num */
	uint32_t rx_num_ch, tx_num_ch;
};

struct ipm_mhuv2_device_config {
	uint8_t *rx_base;
	uint8_t *tx_base;
	void (*rx_irq_config_func)(const struct device *d);
	uint32_t tx_chan_conf[IPM_MHUV2_CHAN_CONF_MAX_SIZE];
	uint32_t rx_chan_conf[IPM_MHUV2_CHAN_CONF_MAX_SIZE];
	int tx_chan_conf_size, rx_chan_conf_size;
};

/* Device structure operation */
#define MHUV2_DEV_CFG(dev) \
		((const struct ipm_mhuv2_device_config * const)(dev)->config)
#define MHUV2_DEV_DATA(dev) \
		((struct ipm_mhuv2_device_data *)(dev)->data)

#define INVALID_CH_INDEX (0xFFFFFFFF)

#endif /* IPM_MHUV2_H */
