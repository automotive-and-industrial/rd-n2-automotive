# Copyright (c) 2022-2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

require recipes-devtools/fvp/fvp-ecosystem.inc

MODEL = "Neoverse-N2-Automotive"
MODEL_CODE = "FVP_RD_N2_Automotive"

FVP_RD_N2_AUTOMOTIVE_URI ?= "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Automotive%20FVPs/${MODEL_CODE}_${PV_URL}_${FVP_ARCH}.tgz"

SRC_URI = "${FVP_RD_N2_AUTOMOTIVE_URI};subdir=${BP}"
SRC_URI[sha256sum] = "20c809defe115bff0aa821baca144b074bb4548da56ccba542405abb0f5e5d36"

LIC_FILES_CHKSUM = "file://license_terms/license_agreement.txt;md5=1a33828e132ba71861c11688dbb0bd16 \
                    file://license_terms/third_party_licenses/third_party_licenses.txt;md5=34a1ba318d745f05e6197def68ea5411 \
                    file://license_terms/third_party_licenses/arm_license_management_utilities/third_party_licenses.txt;md5=2e53bda6ff2db4c35d69944b93926c9f"
