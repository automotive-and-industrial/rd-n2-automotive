# Copyright (c) 2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

PV = "git+${SRCPV}"
# inclusivity-exception
SRCBRANCH = "master"
SRCREV = "a1e8602d5ec2acad923b24eb436dc0f663eea649"
