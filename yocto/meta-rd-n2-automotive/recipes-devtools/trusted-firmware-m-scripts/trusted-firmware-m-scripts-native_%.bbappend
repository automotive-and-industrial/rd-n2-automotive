# Copyright (c) 2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Include machine specific TFM scripts configurations

MACHINE_TFM_SCRIPTS_REQUIRE ?= ""
MACHINE_TFM_SCRIPTS_REQUIRE:class-native = \
    "trusted-firmware-m-scripts-native.inc"

require ${MACHINE_TFM_SCRIPTS_REQUIRE}
