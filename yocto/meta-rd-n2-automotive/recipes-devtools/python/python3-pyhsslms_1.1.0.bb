# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Pure-Python implementation of HSS/LMS Digital Signatures (RFC 8554)"
HOMEPAGE ="https://pypi.org/project/pyhsslms"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=58f6f7065b99f9d01d56e759256a6f1b"

inherit pypi setuptools3
PYPI_PACKAGE = "pyhsslms"
SRC_URI[sha256sum] = "c9e237cb91a31e229be1b20beb2b4970112bd11bddbe55749f6eb6e2d2992ea0"

BBCLASSEXTEND = "native nativesdk"
