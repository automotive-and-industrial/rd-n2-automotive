# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

GENERIC_ARM64_VIRTUALIZATION_EXTRAS = \
"${RD_N2_AUTOMOTIVE_VIRTUALIZATION_DYNAMIC_DIR}/conf/machine/generic-arm64-xen.inc"
require ${@bb.utils.contains('DISTRO_FEATURES', 'xen', \
    d.getVar('GENERIC_ARM64_VIRTUALIZATION_EXTRAS'), '', d)}
