#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

INHIBIT_DEFAULT_DEPS = "1"
COMPATIBLE_HOST = "aarch64-*"
PACKAGE_ARCH = "${TARGET_ARCH}"
inherit nopackages deploy rootfs-postcommands ${IMAGE_CLASSES} python3native

do_patch() {
    # U-Boot's EFI variable persistence implementation stores the variables in
    # a file on the "EFI System Partition" (ESP), if it exists. The boot
    # partition of the SystemReady ACS image is not an ESP, so change the
    # partition type here.
    parted "${WORKDIR}/${IMAGE_FILENAME}" set 1 esp on
}
do_patch[depends] = "parted-native:do_populate_sysroot"

do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_install[noexec] = "1"

# Deploy with this suffix so it is picked up in the machine configuration
IMAGE_DEPLOY_SUFFIX ?= ".wic"

python do_deploy() {
    suffix = d.getVar('IMAGE_DEPLOY_SUFFIX')
    imgfile = os.path.join(d.getVar('WORKDIR'), d.getVar('IMAGE_FILENAME'))
    deployfile = os.path.join(d.getVar('DEPLOYDIR'),
                              d.getVar('IMAGE_NAME') + suffix)
    linkfile = os.path.join(d.getVar('DEPLOYDIR'),
                            d.getVar('IMAGE_LINK_NAME') + suffix)

    # Install the image file in the deploy directory
    import shutil
    shutil.copyfile(imgfile, deployfile)
    if os.path.lexists(linkfile):
        os.remove(manifest_link)
    os.symlink(os.path.basename(deployfile), linkfile)

    # Ensure an empty rootfs manifest exists (required by testimage)
    fname = os.path.join(d.getVar('DEPLOYDIR'),
                         d.getVar('IMAGE_LINK_NAME') + ".manifest")
    open(fname, 'w').close()
}
addtask deploy after do_install before do_image_complete

# Post-process commands may write to IMGDEPLOYDIR
IMGDEPLOYDIR = "${DEPLOY_DIR_IMAGE}"
# Write the test data in IMAGE_POSTPROCESS_COMMAND
IMAGE_POSTPROCESS_COMMAND += "write_image_test_data; "

python do_image_complete() {
    # Run the image post-process commands
    from oe.utils import execute_pre_post_process
    post_process_cmds = d.getVar("IMAGE_POSTPROCESS_COMMAND")
    execute_pre_post_process(d, post_process_cmds)
}
do_image_complete[nostamp] = "1"
addtask image_complete after do_deploy before do_build

python() {
    def extraimage_getdepends(task):
        deps = ""
        for dep in (d.getVar('EXTRA_IMAGEDEPENDS') or "").split():
            if ":" in dep:
                deps += " %s " % (dep)
            else:
                deps += " %s:%s" % (dep, task)
        return deps
    d.appendVarFlag('do_image_complete', 'depends',
                    extraimage_getdepends('do_populate_sysroot'))
}

ACS_LOG_NAME = "acs_results_${DATETIME}"
ACS_LOG_NAME[vardepsexclude] += "DATETIME"
ACS_LOG_DIR = "${TEST_LOG_DIR}/${ACS_LOG_NAME}"
ACS_LOG_LINK = "${TEST_LOG_DIR}/acs_results"
ACS_BASELINE_MACHINE ?= "${MACHINE}"
RM_WORK_EXCLUDE_ITEMS += "${@ os.path.basename(d.getVar('TEST_LOG_DIR')) }"

do_testimage[postfuncs] += "acs_logs_handle"
do_testimage[depends] += "edk2-test-parser-native:do_populate_sysroot \
                          systemready-scripts-native:do_populate_sysroot"

# Process the logs
python acs_logs_handle() {
    import acs.analyze
    import logging
    from oeqa.utils import make_logger_bitbake_compatible

    systemready_scripts_dir = os.path.join(d.getVar('STAGING_LIBDIR_NATIVE'),
                                           "systemready_scripts")
    edk2_test_parser_dir = os.path.join(d.getVar('STAGING_LIBDIR_NATIVE'),
                                        "edk2_test_parser")
    deployfile = os.path.join(d.getVar('DEPLOY_DIR_IMAGE'),
                              d.getVar('IMAGE_LINK_NAME')
                              + d.getVar('IMAGE_DEPLOY_SUFFIX'))

    logdir = d.getVar('ACS_LOG_DIR')
    loglink = d.getVar('ACS_LOG_LINK')

    # Extract the log files from the Wic image to the testimage logs directory
    resultspath = deployfile + ':2/acs_results'
    import subprocess
    subprocess.run(['wic', 'cp', resultspath, logdir], check=True)

    # Create a symlink to the acs_results directory
    if os.path.lexists(loglink):
        os.remove(loglink)
    os.symlink(os.path.basename(logdir), loglink)

    # Create a top-level symlink to the acs_results directory
    top_logdir = os.path.join(get_testimage_json_result_dir(d), d.getVar("PN"))
    log_name = d.getVar('ACS_LOG_NAME')
    top_link = os.path.join(top_logdir, log_name)
    log_target = os.path.relpath(logdir, top_logdir)
    os.symlink(log_target, top_link)

    # Parse and analyze the logs
    logger = make_logger_bitbake_compatible(logging.getLogger("BitBake"))

    sct_log = os.path.join(logdir, 'sct_results', 'Overall', 'Summary.ekl')
    sct_seq = os.path.join(logdir, 'sct_results', 'Sequence', 'EBBR.seq')

    parser_path = os.path.join(edk2_test_parser_dir, "parser.py")
    # format-sr-results.py needs the output file to be called "result.md"
    subprocess.run([parser_path, sct_log, sct_seq, "--md",
                    os.path.join(logdir, "result.md")], check=True)

    scripts_path = os.path.join(systemready_scripts_dir,
                                "format-sr-results.py")
    subprocess.run([scripts_path, "--dir", os.path.split(logdir)[0], "--md", \
                    os.path.join(logdir, "summary.md")], check=True)

    baseline_machine = d.getVar('ACS_BASELINE_MACHINE')
    acs.analyze.compare_with_baseline(logdir, baseline_machine, logger, logdir)
}
