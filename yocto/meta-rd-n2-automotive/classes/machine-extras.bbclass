# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Used to apply configs for machine specific features based on the selected
# MACHINE

include conf/machine/${MACHINE}-machine-extras.inc
