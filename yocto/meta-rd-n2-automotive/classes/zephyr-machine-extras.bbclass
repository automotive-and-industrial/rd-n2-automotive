# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

include recipes-kernel/zephyr-kernel/zephyr-${MACHINE}.inc
