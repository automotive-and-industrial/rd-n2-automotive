# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Build a raw disk image using srec_cat, which concatenates the provided
# binaries at the given offsets into a single file.
# To use, provide the list of binaries as varflags to SRECORD_IMAGE_PARTS,
# where the name is the offset and the value in the filename of the binary in
# the sysroot.
# Recipes using this class are responsible to ensure that the binaries are
# present in the sysroot, e.g. by adding the relevant recipe(s) to DEPENDS.

LICENSE = "MIT"
LIC_FILES_CHKSUM = "\
    file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
    "

DEPENDS += "srecord-native"
PACKAGE_ARCH = "${MACHINE_ARCH}"

SRECORD_IMAGE_OUTPUT_NAME ?= "${PN}-${MACHINE}.bin"

do_configure[noexec] = "1"

python do_compile() {
    args = ['srec_cat']
    image_parts = d.getVarFlags('SRECORD_IMAGE_PARTS') or {}
    for offset, filename in image_parts.items():
        input_path = d.expand(filename)
        args += [input_path, '-Binary', '-offset', offset]

    output_path = d.getVar('SRECORD_IMAGE_OUTPUT_NAME')
    args += ['-o', output_path, '-Binary']

    import subprocess
    subprocess.run(args, check=True)
}
do_compile[vardeps] += "SRECORD_IMAGE_PARTS"

do_install() {
    install -d ${D}/firmware
    install ${B}/${SRECORD_IMAGE_OUTPUT_NAME} ${D}/firmware/${SRECORD_IMAGE_OUTPUT_NAME}
}

FILES:${PN} = "/firmware"
SYSROOT_DIRS += "/firmware"

inherit deploy

do_deploy() {
    cp -rf ${D}/firmware/${SRECORD_IMAGE_OUTPUT_NAME} ${DEPLOYDIR}/${SRECORD_IMAGE_OUTPUT_NAME}
}
addtask deploy after do_install

python() {
    if not d.getVarFlags('SRECORD_IMAGE_PARTS'):
        raise bb.SkipRecipe("At least one SRECORD_IMAGE_PARTS varflag must be"
                            " defined")
}
