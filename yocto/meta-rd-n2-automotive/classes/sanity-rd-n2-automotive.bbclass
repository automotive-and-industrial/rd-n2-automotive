# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

addhandler rdn2_auto_machinecheck

rdn2_auto_machinecheck[eventmask] = "bb.event.SanityCheck"

python rdn2_auto_machinecheck() {
    skip_check = e.data.getVar('SKIP_META_RDN2_AUTO_MACHINE_CHECK') == "1"
    if 'fvp-rd-n2-automotive' not in e.data.getVar('MACHINE') and \
            not e.data.getVar('BBMULTICONFIG') and \
            not skip_check:
        bb.warn("The meta-rd-n2-automotive layer is included, but "
                "'fvp-rd-n2-automotive' has not been set as the MACHINE. Some "
                " recipes and bbappend files may not take effect.")
}
