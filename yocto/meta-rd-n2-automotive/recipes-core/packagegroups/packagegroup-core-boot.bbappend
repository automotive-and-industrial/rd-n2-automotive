#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Don't include any EFI loader files in rootfs /boot path
RDEPENDS:${PN}:remove:fvp-rd-n2-automotive = "${EFI_PROVIDER}"
