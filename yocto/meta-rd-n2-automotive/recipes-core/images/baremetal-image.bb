#
# Copyright (c) 2022-2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Baremetal image"
DESCRIPTION = "An image recipe, based on core-image"

inherit core-image

IMAGE_INSTALL = "\
    packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL} \
    packagegroup-machine-base \
    packagegroup-core-ssh-openssh \
    system-tests-ptest \
    iperf \
"
IMAGE_LINGUAS = ""

inherit features_check
CONFLICT_DISTRO_FEATURES = "xen"
