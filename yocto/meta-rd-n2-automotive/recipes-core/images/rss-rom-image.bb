# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "RSS ROM Image"
DESCRIPTION = "Image to load at boot in the RSS ROM. It contains BL1 from \
Trusted Firmware-M"

inherit srecord_image

COMPATIBLE_MACHINE = "fvp-rd-n2-automotive"
DEPENDS += "trusted-firmware-m"

SRECORD_IMAGE_PARTS[0x0] = "${RECIPE_SYSROOT}/firmware/bl1_1.bin"
SRECORD_IMAGE_PARTS[0xE000] = "${RECIPE_SYSROOT}/firmware/bl1_provisioning_bundle.bin"

INSANE_SKIP:${PN} = "buildpaths"
