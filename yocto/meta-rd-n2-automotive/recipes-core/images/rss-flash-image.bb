# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "RSS Flash Image"
DESCRIPTION = "Image to load at boot in Safety Island NVM storage. It \
contains the images for TF-M BL2, TF-A BL1, SCP BL1 and the Safety Island"

inherit tfm_sign_image srecord_image

COMPATIBLE_MACHINE = "fvp-rd-n2-automotive"
DEPENDS += "\
    trusted-firmware-m \
    scp-firmware \
    trusted-firmware-a \
    ${SAFETY_ISLAND_IMAGE} \
    "

RE_LAYOUT_WRAPPER_VERSION = "0.0.7"
RE_IMAGE_OFFSET = "0x1000"
TFM_SIGN_PRIVATE_KEY = "${libdir}/tfm-scripts/root-RSA-3072.pem"
SCP_FIRMWARE_BINARY = "scp_romfw.bin"
SCP_FIRMWARE_IMAGE_LOAD_ADDRESS = "0x70400000"
SCP_FIRMWARE_SIGN_BIN_SIZE = "0x10000"
TFA_BL1_BINARY = "bl1-rdn2.bin"
TFA_BL1_IMAGE_LOAD_ADDRESS = "0x70000000"
TFA_BL1_SIGN_BIN_SIZE = "0x20000"
SAFETY_ISLAND_BINARY = "${SAFETY_ISLAND_IMAGE}.bin"
SAFETY_ISLAND_IMAGE_LOAD_ADDRESS = "0x70100000"
SAFETY_ISLAND_SIGN_BIN_SZE = "0x300000"

do_sign_images() {
    sign_host_image ${RECIPE_SYSROOT}/firmware/${SCP_FIRMWARE_BINARY} \
        ${SCP_FIRMWARE_IMAGE_LOAD_ADDRESS} ${SCP_FIRMWARE_SIGN_BIN_SIZE}
    sign_host_image ${RECIPE_SYSROOT}/firmware/${TFA_BL1_BINARY} \
        ${TFA_BL1_IMAGE_LOAD_ADDRESS} ${TFA_BL1_SIGN_BIN_SIZE}
    sign_host_image ${DEPLOY_DIR_IMAGE}/${SAFETY_ISLAND_BINARY} \
        ${SAFETY_ISLAND_IMAGE_LOAD_ADDRESS} ${SAFETY_ISLAND_SIGN_BIN_SZE}
}

SRECORD_IMAGE_PARTS[0x000000] = "${RECIPE_SYSROOT}/firmware/bl2_signed.bin"
SRECORD_IMAGE_PARTS[0x020000] = "${RECIPE_SYSROOT}/firmware/bl2_signed.bin"
SRECORD_IMAGE_PARTS[0x040000] = "${RECIPE_SYSROOT}/firmware/tfm_s_ns_signed.bin"
SRECORD_IMAGE_PARTS[0x140000] = "${RECIPE_SYSROOT}/firmware/tfm_s_ns_signed.bin"
SRECORD_IMAGE_PARTS[0x240000] = "${TFM_IMAGE_SIGN_DIR}/signed_bl1-rdn2.bin"
SRECORD_IMAGE_PARTS[0x2C0000] = "${TFM_IMAGE_SIGN_DIR}/signed_${SAFETY_ISLAND_IMAGE}.bin"
SRECORD_IMAGE_PARTS[0x5C0000] = "${TFM_IMAGE_SIGN_DIR}/signed_bl1-rdn2.bin"
SRECORD_IMAGE_PARTS[0x640000] = "${TFM_IMAGE_SIGN_DIR}/signed_${SAFETY_ISLAND_IMAGE}.bin"
SRECORD_IMAGE_PARTS[0x940000] = "${TFM_IMAGE_SIGN_DIR}/signed_scp_romfw.bin"
SRECORD_IMAGE_PARTS[0x9C0000] = "${TFM_IMAGE_SIGN_DIR}/signed_scp_romfw.bin"

INSANE_SKIP:${PN} = "buildpaths"
