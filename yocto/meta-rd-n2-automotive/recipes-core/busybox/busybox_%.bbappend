# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

# As Bats requires the nl utility, configure CONFIG_NL=y for busybox if we are
# using Bats
SRC_URI:append:fvp-rd-n2-automotive = "file://nl.cfg"
SRC_URI:append:generic-arm64 = "file://nl.cfg"
