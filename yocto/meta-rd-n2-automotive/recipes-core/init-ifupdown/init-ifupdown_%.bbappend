# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

MACHINE_INIT_IFUPDOWN_REQUIRE ?= ""
MACHINE_INIT_IFUPDOWN_REQUIRE:fvp-rd-n2-automotive = \
    "init-ifupdown-extras.inc"
MACHINE_INIT_IFUPDOWN_REQUIRE:generic-arm64 = \
    "init-ifupdown-extras.inc"

require ${MACHINE_INIT_IFUPDOWN_REQUIRE}
