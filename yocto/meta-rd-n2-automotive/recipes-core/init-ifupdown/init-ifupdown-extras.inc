# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://interfaces.d/0001-external \
    ${@bb.utils.contains('DISTRO_FEATURES', 'xen', '', 'file://interfaces.d/0002-safety-island', d)} \
    "

install_interfaces() {
    cp -rf ${WORKDIR}/interfaces.d ${D}${sysconfdir}/network/
}
do_install[postfuncs] += "install_interfaces"
