# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Device tree"
DESCRIPTION = "Device tree for the fvp-rd-n2-automotive"

inherit devicetree

COMPATIBLE_MACHINE = "fvp-rd-n2-automotive"
FILESEXTRAPATHS:prepend := "\
${RD_N2_AUTOMOTIVE_REPO_DIRECTORY}/components/primary_compute/devicetree:"
SRC_URI = "file://fvp-rd-n2-automotive.dts"
