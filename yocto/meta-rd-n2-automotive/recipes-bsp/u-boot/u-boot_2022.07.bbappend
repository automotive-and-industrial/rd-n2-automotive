# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Include machine specific u-boot configuration

MACHINE_UBOOT_REQUIRE ?= ""
MACHINE_UBOOT_REQUIRE:fvp-rd-n2-automotive = \
    "u-boot-fvp-rd-n2-automotive.inc"

require ${MACHINE_UBOOT_REQUIRE}
