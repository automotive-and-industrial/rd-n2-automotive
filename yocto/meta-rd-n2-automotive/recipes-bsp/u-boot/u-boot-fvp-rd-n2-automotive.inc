# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

COMPATIBLE_MACHINE       = "fvp-rd-n2-automotive"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI += "\
    file://0001-vexpress64-Change-prior-stage-fdt-register-to-x1.patch \
    file://0002-configs-Create-minimal-vexpress_fvp_defconfig.patch \
    file://0003-vexpress64-Enable-VIRTIO_MMIO-and-RTC_PL031-in-the-b.patch \
    "
SRC_URI:append = " file://rdn2.cfg"
