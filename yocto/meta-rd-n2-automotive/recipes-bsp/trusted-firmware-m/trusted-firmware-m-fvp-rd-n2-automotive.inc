# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

PV = "git+${SRCPV}"
SRCREV_FORMAT = "tfm"
DEPENDS += "python3-pyhsslms-native"

# TF-M is running on an M-core alongside the AArch64 A/R cores
INSANE_SKIP:${PN} = "arch"

# TF-M
# inclusivity-exception
SRCBRANCH_tfm = "master"
SRCREV_tfm = "a1e8602d5ec2acad923b24eb436dc0f663eea649"
# mbedtls-3.2.1
SRCREV_mbedtls = "869298bffeea13b205343361b7a7daf2b210e33d"
# TF-M Tests
# inclusivity-exception
SRCBRANCH_tfm-tests = "master"
SRCREV_tfm-tests = "5c8b639f0392699747fd1a07d6bc301a90f4fe9c"
# MCUboot v1.9.0
SRCREV_mcuboot = "c657cbea75f2bb1faf1fceacf972a0537a8d26dd"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI_EXTRA_PATCHES = "file://0001-RSS-Skip-loading-SCP-and-AP.patch \
    file://0002-RSS-Load-and-boot-SCP-BL1.patch \
    file://0003-RSS-Update-SCP-RSS-MHUv2-base.patch \
    file://0004-RSS-Clear-after-receiving-Doorbell-from-SCP.patch \
    file://0005-RSS-Replace-RSS-flash-with-SI-NVM-flash.patch \
    file://0006-RSS-Load-AP-BL1.patch \
    file://0007-RSS-Load-and-boot-SI-CL0.patch \
    file://0008-RSS-Enable-the-dedicated-UART-for-RSS.patch \
    "
SRC_URI += "${SRC_URI_EXTRA_PATCHES}"

EXTRA_OECMAKE:append = " -DMCUBOOT_IMAGE_NUMBER=5"

TFM_DEBUG = "1"
TFM_PLATFORM = "arm/rss"

# Only install the required binaries
do_install() {
    install -D -p -m 0644 ${B}/install/outputs/bl1_1.bin ${D}/firmware/bl1_1.bin
    install -D -p -m 0644 ${B}/install/outputs/bl1_provisioning_bundle.bin ${D}/firmware/bl1_provisioning_bundle.bin
    install -D -p -m 0644 ${B}/install/outputs/bl2_signed.bin ${D}/firmware/bl2_signed.bin
    install -D -p -m 0644 ${B}/install/outputs/tfm_s_ns_signed.bin ${D}/firmware/tfm_s_ns_signed.bin
}
INSANE_SKIP:${PN} = "buildpaths"
