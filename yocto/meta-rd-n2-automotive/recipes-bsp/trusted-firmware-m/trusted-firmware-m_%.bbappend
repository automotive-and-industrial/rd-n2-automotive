# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Include machine specific TFM configurations

MACHINE_TFM_REQUIRE ?= ""
MACHINE_TFM_REQUIRE:fvp-rd-n2-automotive = \
    "trusted-firmware-m-fvp-rd-n2-automotive.inc"

require ${MACHINE_TFM_REQUIRE}
