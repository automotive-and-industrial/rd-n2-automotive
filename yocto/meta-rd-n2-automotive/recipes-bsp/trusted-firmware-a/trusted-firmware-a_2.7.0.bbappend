# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Include machine specific TFA configurations

MACHINE_TFA_REQUIRE ?= ""
MACHINE_TFA_REQUIRE:fvp-rd-n2-automotive = \
    "trusted-firmware-a-fvp-rd-n2-automotive.inc"

require ${MACHINE_TFA_REQUIRE}
