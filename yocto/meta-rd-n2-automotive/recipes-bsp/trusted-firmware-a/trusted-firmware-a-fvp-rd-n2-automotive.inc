# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# SRCREV_tfa SHA points to RD-INFRA-2022.08.18 tag
SRCREV_tfa = "61f5895593cdd601b159ca817daef84bfc8fb3b5"
PV .= "+git${SRCPV}"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI = "\
    git://git.gitlab.arm.com/arm-reference-solutions/trusted-firmware-a.git;protocol=https;name=tfa;nobranch=1 \
    file://0001-feat-board-rdn2-Add-HW_CONFIG-device-tree-support.patch \
    file://0002-feat-board-rdn2-Fixup-HW_CONFIG-device-tree-in-BL2.patch \
    file://0003-feat-sgi-configure-SRAM-and-BL31-size-for-sgi-platfo.patch \
    file://0004-feat-sgi-remove-override-for-ARM_BL31_IN_DRAM-build-.patch \
    "
SRC_URI:append = " file://rwx-segments.patch"

LIC_FILES_CHKSUM += "file://docs/license.rst;md5=b2c740efedc159745b9b31f88ff03dde"

COMPATIBLE_MACHINE           = "fvp-rd-n2-automotive"
TFA_PLATFORM                 = "rdn2"
TFA_BUILD_TARGET             = "bl1 bl31 dtbs fip"
TFA_INSTALL_TARGET           = "bl1 fip.bin"
TFA_DEBUG                    = "1"
TFA_MBEDTLS                  = "0"

TFA_UBOOT = "1"

DEPENDS += "virtual/dtb"
EXTRA_OEMAKE += "\
    CSS_SGI_PLATFORM_VARIANT=1 \
    HW_CONFIG=${RECIPE_SYSROOT}/boot/devicetree/fvp-rd-n2-automotive.dtb \
    ENABLE_MPAM_FOR_LOWER_ELS=1 \
    "
