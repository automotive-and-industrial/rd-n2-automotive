# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "SystemReady Firmware Image"
DESCRIPTION = "This recipe ensures that all packages listed in \
SYSTEMREADY_FIRMWARE variable (set at machine conf) are deployed."

EXTRA_IMAGEDEPENDS = "${SYSTEMREADY_FIRMWARE}"

inherit extra_imagedepends_only
