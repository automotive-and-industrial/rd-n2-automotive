# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Include machine specific SCP configurations

MACHINE_SCP_REQUIRE ?= ""
MACHINE_SCP_REQUIRE:fvp-rd-n2-automotive = \
    "scp-firmware-fvp-rd-n2-automotive.inc"

require ${MACHINE_SCP_REQUIRE}
