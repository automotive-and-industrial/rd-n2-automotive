# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# SRCREV SHA points to "RD-INFRA-2022.08.18" tag.
SRCREV = "20fb59e930212dcff78c2c647c5917819a0de653"
PV .= "+git${SRCPV}"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI = "\
    gitsm://git.gitlab.arm.com/arm-reference-solutions/scp-firmware.git;protocol=https;nobranch=1 \
    file://0001-product-rd-n2-Replace-smt-with-transport.patch \
    file://0002-product-rdn2-Add-a-new-module-for-boot-flow.patch \
    file://0003-product-rdn2-Configure-MHU2-and-Transport-module.patch \
    file://0004-product-rdn2-RSS-based-boot-flow.patch \
    file://0005-product-rdn2-Implement-AP-Boot-Flow.patch \
    file://0006-product-rdn2-Reset-SI-CL0-on-demand.patch \
    file://0007-product-rdn2-Distinguish-RD-N2-Auto-and-RD-N2.patch \
    file://0008-product-rdn2-Halt-Safety-Island-CPU-during-system-shutdown.patch \
    file://0009-product-rdn2-Replace-CPUHALT-with-PPU-on-SI.patch \
    "

COMPATIBLE_MACHINE     = "fvp-rd-n2-automotive"
SCP_BUILD_RELEASE      = "0"
SCP_PLATFORM           = "rdn2"
SCP_LOG_LEVEL          = "INFO"
FW_TARGETS             = "scp"

EXTRA_OECMAKE += "-DSCP_PLATFORM_VARIANT=1 -DSCP_ENABLE_RSS_BOOT_FLOW=TRUE"

# The development branch has added the .elf extension to the debug symbols, so
# override do_install here
do_install() {
     install -d ${D}/firmware
     for TYPE in ${FW_INSTALL}; do
         for FW in ${FW_TARGETS}; do
            if [ "$TYPE" = "romfw" ]; then
                if [ "$FW" = "scp" ]; then
                    install -D "${B}/${TYPE}/${FW}/bin/${SCP_PLATFORM}-bl1.bin" "${D}/firmware/${FW}_${TYPE}.bin"
                    install -D "${B}/${TYPE}/${FW}/bin/${SCP_PLATFORM}-bl1.elf" "${D}/firmware/${FW}_${TYPE}.elf"
                elif [ "$FW" = "mcp" ]; then
                    install -D "${B}/${TYPE}/${FW}/bin/${SCP_PLATFORM}-mcp-bl1.bin" "${D}/firmware/${FW}_${TYPE}.bin"
                    install -D "${B}/${TYPE}/${FW}/bin/${SCP_PLATFORM}-mcp-bl1" "${D}/firmware/${FW}_${TYPE}.elf"
                fi
            elif [ "$TYPE" = "ramfw" ]; then
                if [ "$FW" = "scp" ]; then
                    install -D "${B}/${TYPE}/${FW}/bin/${SCP_PLATFORM}-bl2.bin" "${D}/firmware/${FW}_${TYPE}.bin"
                    install -D "${B}/${TYPE}/${FW}/bin/${SCP_PLATFORM}-bl2.elf" "${D}/firmware/${FW}_${TYPE}.elf"
                elif [ "$FW" = "mcp" ]; then
                    install -D "${B}/${TYPE}/${FW}/bin/${SCP_PLATFORM}-mcp-bl2.bin" "${D}/firmware/${FW}_${TYPE}.bin"
                    install -D "${B}/${TYPE}/${FW}/bin/${SCP_PLATFORM}-mcp-bl2.elf" "${D}/firmware/${FW}_${TYPE}.elf"
                fi
            fi
        done
     done
}
