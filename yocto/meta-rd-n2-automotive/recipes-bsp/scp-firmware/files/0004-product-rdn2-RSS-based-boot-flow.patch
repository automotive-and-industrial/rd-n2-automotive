From d35b0810044dfcc44f3199011559b75a916dbb15 Mon Sep 17 00:00:00 2001
From: Xinxu Chen <xinxu.chen@arm.com>
Date: Thu, 25 Aug 2022 10:23:06 +0800
Subject: [PATCH 4/8] product/rdn2: RSS based boot flow

SCP sends a Doorbell to RSS to indicate the liveness after module
`boot_flow` started.

RSS will send a Doorbell to instruct SCP to boot a PE.
`boot_flow` module receives the Doorbell and send an event to
`platform_system` for action.

Issue-Id: SCM-5129
Signed-off-by: Xinxu Chen <xinxu.chen@arm.com>
Signed-off-by: Peter Hoyes <Peter.Hoyes@arm.com>
Upstream-Status: Pending
---
 product/rdn2/module/boot_flow/CMakeLists.txt  |  2 +-
 .../module/boot_flow/include/mod_boot_flow.h  |  5 +++
 .../rdn2/module/boot_flow/src/mod_boot_flow.c | 40 ++++++++++++++-----
 .../module/platform_system/CMakeLists.txt     |  3 +-
 .../include/mod_platform_system.h             |  8 +++-
 .../platform_system/src/mod_platform_system.c | 23 +++++++++++
 6 files changed, 68 insertions(+), 13 deletions(-)

diff --git a/product/rdn2/module/boot_flow/CMakeLists.txt b/product/rdn2/module/boot_flow/CMakeLists.txt
index 9db46ca2..39ffd5be 100644
--- a/product/rdn2/module/boot_flow/CMakeLists.txt
+++ b/product/rdn2/module/boot_flow/CMakeLists.txt
@@ -14,5 +14,5 @@ target_sources(${SCP_MODULE_TARGET}
                PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/src/mod_boot_flow.c")
 
 target_link_libraries(
-    ${SCP_MODULE_TARGET} PRIVATE  module-power-domain
+    ${SCP_MODULE_TARGET} PRIVATE  module-power-domain module-platform-system
                                  module-ppu-v1 module-transport)
diff --git a/product/rdn2/module/boot_flow/include/mod_boot_flow.h b/product/rdn2/module/boot_flow/include/mod_boot_flow.h
index 26f13adf..ac210780 100644
--- a/product/rdn2/module/boot_flow/include/mod_boot_flow.h
+++ b/product/rdn2/module/boot_flow/include/mod_boot_flow.h
@@ -28,6 +28,11 @@
 struct mod_boot_flow_config {
 };
 
+enum mod_boot_flow_event {
+    MOD_BOOT_FLOW_EVENT_PLATFORM_SYSTEM_READY,
+    MOD_BOOT_FLOW_EVENT_COUNT
+};
+
 /*!
  * \brief Type of the interfaces exposed by the module.
  */
diff --git a/product/rdn2/module/boot_flow/src/mod_boot_flow.c b/product/rdn2/module/boot_flow/src/mod_boot_flow.c
index a9a63f39..d5a71606 100644
--- a/product/rdn2/module/boot_flow/src/mod_boot_flow.c
+++ b/product/rdn2/module/boot_flow/src/mod_boot_flow.c
@@ -6,8 +6,9 @@
  *
  */
 
-#include <mod_boot_flow.h>
+#include <mod_platform_system.h>
 #include <mod_transport.h>
+#include <mod_boot_flow.h>
 
 #include <fwk_assert.h>
 #include <fwk_core.h>
@@ -28,11 +29,6 @@ struct boot_flow_ctx {
     unsigned int notification_count; /* Notifications awaiting a response */
 } ctx;
 
-enum mod_boot_flow_event {
-    MOD_BOOT_FLOW_EVENT_COUNT
-};
-
-
 static int boot_flow_signal_error(fwk_id_t service_id)
 {
     fwk_unexpected();
@@ -41,22 +37,46 @@ static int boot_flow_signal_error(fwk_id_t service_id)
 
 static int boot_flow_signal_message_boot_ap(fwk_id_t service_id)
 {
-    return FWK_SUCCESS;
+    struct fwk_event event = {
+        .target_id = FWK_ID_MODULE(FWK_MODULE_IDX_PLATFORM_SYSTEM),
+        .source_id = FWK_ID_MODULE(FWK_MODULE_IDX_BOOT_FLOW),
+        .id = FWK_ID_EVENT(FWK_MODULE_IDX_PLATFORM_SYSTEM,
+                           MOD_PLATFORM_SYSTEM_EVENT_BOOT_AP),
+    };
+    return fwk_put_event(&event);
 }
 
 static int boot_flow_signal_message_boot_sicl0(fwk_id_t service_id)
 {
-    return FWK_SUCCESS;
+    struct fwk_event event = {
+        .target_id = FWK_ID_MODULE(FWK_MODULE_IDX_PLATFORM_SYSTEM),
+        .source_id = FWK_ID_MODULE(FWK_MODULE_IDX_BOOT_FLOW),
+        .id = FWK_ID_EVENT(FWK_MODULE_IDX_PLATFORM_SYSTEM,
+                           MOD_PLATFORM_SYSTEM_EVENT_BOOT_SICL0),
+    };
+    return fwk_put_event(&event);
 }
 
 static int boot_flow_signal_message_boot_sicl1(fwk_id_t service_id)
 {
-    return FWK_SUCCESS;
+    struct fwk_event event = {
+        .target_id = FWK_ID_MODULE(FWK_MODULE_IDX_PLATFORM_SYSTEM),
+        .source_id = FWK_ID_MODULE(FWK_MODULE_IDX_BOOT_FLOW),
+        .id = FWK_ID_EVENT(FWK_MODULE_IDX_PLATFORM_SYSTEM,
+                           MOD_PLATFORM_SYSTEM_EVENT_BOOT_SICL1),
+    };
+    return fwk_put_event(&event);
 }
 
 static int boot_flow_signal_message_boot_sicl2(fwk_id_t service_id)
 {
-    return FWK_SUCCESS;
+    struct fwk_event event = {
+        .target_id = FWK_ID_MODULE(FWK_MODULE_IDX_PLATFORM_SYSTEM),
+        .source_id = FWK_ID_MODULE(FWK_MODULE_IDX_BOOT_FLOW),
+        .id = FWK_ID_EVENT(FWK_MODULE_IDX_PLATFORM_SYSTEM,
+                           MOD_PLATFORM_SYSTEM_EVENT_BOOT_SICL2),
+    };
+    return fwk_put_event(&event);
 }
 
 static const struct mod_transport_firmware_signal_api
diff --git a/product/rdn2/module/platform_system/CMakeLists.txt b/product/rdn2/module/platform_system/CMakeLists.txt
index d2eee4a5..099dca1d 100644
--- a/product/rdn2/module/platform_system/CMakeLists.txt
+++ b/product/rdn2/module/platform_system/CMakeLists.txt
@@ -35,4 +35,5 @@ target_sources(${SCP_MODULE_TARGET}
 
 target_link_libraries(${SCP_MODULE_TARGET}
     PRIVATE module-sds module-clock module-power-domain module-ppu-v1
-            module-scmi module-system-power module-apremap module-system-info)
+            module-boot-flow module-scmi module-system-power module-apremap
+            module-system-info)
diff --git a/product/rdn2/module/platform_system/include/mod_platform_system.h b/product/rdn2/module/platform_system/include/mod_platform_system.h
index fb660fcf..767e6edb 100644
--- a/product/rdn2/module/platform_system/include/mod_platform_system.h
+++ b/product/rdn2/module/platform_system/include/mod_platform_system.h
@@ -22,7 +22,13 @@
  * \defgroup GroupPLATFORMSystem PLATFORM System Support
  * @{
  */
-
+enum platform_system_event {
+    MOD_PLATFORM_SYSTEM_EVENT_BOOT_AP,
+    MOD_PLATFORM_SYSTEM_EVENT_BOOT_SICL0,
+    MOD_PLATFORM_SYSTEM_EVENT_BOOT_SICL1,
+    MOD_PLATFORM_SYSTEM_EVENT_BOOT_SICL2,
+    MOD_PLATFORM_SYSTEM_EVENT_COUNT
+};
 /*!
  * \brief Additional PLATFORM system power states.
  */
diff --git a/product/rdn2/module/platform_system/src/mod_platform_system.c b/product/rdn2/module/platform_system/src/mod_platform_system.c
index eb19c72e..db835230 100644
--- a/product/rdn2/module/platform_system/src/mod_platform_system.c
+++ b/product/rdn2/module/platform_system/src/mod_platform_system.c
@@ -15,6 +15,7 @@
 #include "scp_css_mmap.h"
 #include "scp_pik.h"
 
+#include <mod_boot_flow.h>
 #include <mod_apremap.h>
 #include <mod_clock.h>
 #include <mod_platform_system.h>
@@ -25,6 +26,7 @@
 #include <mod_system_info.h>
 #include <mod_system_power.h>
 
+#include <fwk_core.h>
 #include <fwk_assert.h>
 #include <fwk_event.h>
 #include <fwk_id.h>
@@ -597,13 +599,34 @@ int platform_system_process_notification(
 
     return FWK_SUCCESS;
 }
+static int platform_system_process_event(
+    const struct fwk_event *event,
+    struct fwk_event *resp)
+{
+    if (fwk_id_get_module_idx(event->source_id) == FWK_MODULE_IDX_BOOT_FLOW)
+        switch (fwk_id_get_event_idx(event->id)) {
+        case MOD_PLATFORM_SYSTEM_EVENT_BOOT_AP:
+            break;
+        case MOD_PLATFORM_SYSTEM_EVENT_BOOT_SICL0:
+            break;
+        case MOD_PLATFORM_SYSTEM_EVENT_BOOT_SICL1:
+            break;
+        case MOD_PLATFORM_SYSTEM_EVENT_BOOT_SICL2:
+            break;
+        default:
+            return FWK_E_PARAM;
+        }
+    return FWK_SUCCESS;
+}
 
 const struct fwk_module module_platform_system = {
     .type = FWK_MODULE_TYPE_DRIVER,
     .api_count = MOD_PLATFORM_SYSTEM_API_COUNT,
+    .event_count = MOD_PLATFORM_SYSTEM_EVENT_COUNT,
     .init = platform_system_mod_init,
     .bind = platform_system_bind,
     .process_bind_request = platform_system_process_bind_request,
     .process_notification = platform_system_process_notification,
+    .process_event = platform_system_process_event,
     .start = platform_system_start,
 };
-- 
2.25.1

