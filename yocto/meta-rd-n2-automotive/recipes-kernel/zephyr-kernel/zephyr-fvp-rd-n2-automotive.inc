# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "\
${THISDIR}/files:\
${RD_N2_AUTOMOTIVE_REPO_DIRECTORY}/components/safety_island/zephyr:\
"

# meta-zephyr uses a local include for the version-specific .inc file so it is
# necessary to re-include it here
require recipes-kernel/zephyr-kernel/zephyr-kernel-src-${PREFERRED_VERSION_zephyr-kernel}.inc

SRC_URI_ZEPHYR_EXTRA_PATCHES ?= "\
    file://zephyr/0001-soc-fvp_aemv8r-Add-Kconfig-symbol-for-MPU-configurat.patch;patchdir=zephyr \
    file://zephyr/0002-sample-net-fix-incorrect-statistics-of-zperf.patch;patchdir=zephyr \
    "
SRC_URI += "${SRC_URI_ZEPHYR_EXTRA_PATCHES}"

# Add the safety_island Zephyr module
SRC_URI:append = " file://src"
ZEPHYR_SAFETY_ISLAND_MODULE = "${WORKDIR}/src"
ZEPHYR_MODULES:append = "${ZEPHYR_SAFETY_ISLAND_MODULE}\;"
