# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "zperf Zephyr sample application"
DESCRIPTION = "A sample application to demonstrate the zperf shell utility, \
which allows network bandwidth to be evaluated."

require recipes-kernel/zephyr-kernel/zephyr-sample.inc

ZEPHYR_SRC_DIR = "${ZEPHYR_BASE}/samples/net/zperf"

OVERLAY_BASENAME_FILE = "${ZEPHYR_SAFETY_ISLAND_MODULE}/apps/zperf/boards/fvp_rdn2_automotive_cortex_r82_c0"
EXTRA_OECMAKE:append:fvp-rd-n2-automotive = "\
    -DDTC_OVERLAY_FILE=${OVERLAY_BASENAME_FILE}.overlay \
    -DOVERLAY_CONFIG=${OVERLAY_BASENAME_FILE}.conf \
"
