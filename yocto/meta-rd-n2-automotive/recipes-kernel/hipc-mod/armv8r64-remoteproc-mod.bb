# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Armv8r64 remoteproc kernel module"
DESCRIPTION = "A driver for remote communications with an Armv8r64 core using \
shared memory and MHUv2 channels"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

inherit module

FILESEXTRAPATHS:prepend := "${RD_N2_AUTOMOTIVE_REPO_DIRECTORY}/components/primary_compute/linux_drivers/armv8r64_remoteproc_mod:"
SRC_URI = "file://src"
S = "${WORKDIR}/src"

RRECOMMENDS:${PN} += "kernel-module-arm-mhuv2"
