# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}:${THISDIR}/files:"
SRC_URI_EXTRA_PATCHES = "\
    file://0001-rpmsg-virtio-Make-size-and-number-of-buffers-configu.patch \
    file://0002-mailbox-Make-transmit-queue-size-configurable.patch \
    file://0003-virtio-disable-remoteproc-virtio-RPMSG-to-use-DMA-ap.patch \
    "
SRC_URI += "${SRC_URI_EXTRA_PATCHES}"
SRC_URI:append = " file://rd-n2-automotive-kmeta-extra;type=kmeta;name=rd-n2-automotive-kmeta-extra;destsuffix=rd-n2-automotive-kmeta-extra"

KERNEL_FEATURES:append = "\
    features/arm-mhuv2/arm-mhuv2.scc \
    features/rpmsg-virtio/rpmsg-virtio.scc \
    features/mailbox/mailbox.scc \
    "
