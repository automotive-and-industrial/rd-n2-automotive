# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Include machine specific Linux Yocto configurations

MACHINE_LINUX_YOCTO_REQUIRE ?= ""
MACHINE_LINUX_YOCTO_REQUIRE:fvp-rd-n2-automotive = \
    "linux-yocto-fvp-rd-n2-automotive.inc"

require ${MACHINE_LINUX_YOCTO_REQUIRE}
