# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

COMPATIBLE_MACHINE = "fvp-rd-n2-automotive"

KCONFIG_MODE = "--alldefconfig"
KBUILD_DEFCONFIG = "defconfig"

KERNEL_FEATURES:append = "\
    features/random/random.scc \
    cfg/virtio.scc \
    "

require linux-yocto-hipc.inc
