# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

from difflib import HtmlDiff
import pathlib
import os
import filecmp


def load_file(path):
    """
    Return a list containing each of the lines that form the content of a file.
    """
    with open(path) as f:
        return f.readlines()


def generate_html_diff(baseline, new_result, o_path, filename):
    """
    Generate an html file in directory o_path. The html file will contain the
    diff between baseline and new_result.
    """
    new_diff = HtmlDiff().make_file(baseline, new_result, 'baseline',
                                    'new_result', context=True, numlines=3)
    with open(os.path.join(o_path, filename+".html"), 'w') as f:
        f.write(new_diff)


def compare_with_baseline(acs_results_path, baseline_machine, logger, o_path):
    """
    Get the ACS tests summary and compare it with baseline.
    If differences are detected, output different html files with the diff
    in all the different ACS tests types and in the summary.
    Print the comparison conclusion using logger.info.
    """

    acs_results_paths = \
        [
         os.path.join(acs_results_path, 'summary.md'),
         os.path.join(acs_results_path, 'fwts', 'FWTSResults.log'),
         os.path.join(acs_results_path, 'uefi', 'BsaResults.log'),
         os.path.join(acs_results_path, 'result.md')
        ]

    report_names = ['summary.md', 'fwts.log', 'bsa.log', 'sct.md']
    baseline_paths = [pathlib.Path(__file__).parent /
                      f"baseline-{baseline_machine}-{name}"
                      for name in report_names]

    if not filecmp.cmp(baseline_paths[0], acs_results_paths[0], shallow=False):
        logger.info('ACS test suite has different results than baseline.')
        logger.info(f'Please check the diff files in {o_path}/*.html.')

        for bpath, rpath, name in zip(baseline_paths,
                                      acs_results_paths, report_names):
            baseline = load_file(bpath)
            new = load_file(rpath)
            name_no_extension = os.path.splitext(name)[0]
            generate_html_diff(baseline, new, o_path, name_no_extension)

        # This exception fails the task without printing a stack trace
        raise bb.BBHandledException()

    logger.info('ACS test suite results are consistent with baseline.')
