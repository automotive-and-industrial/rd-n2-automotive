# Firmware

TBD

## Setup

TBD

## U-Boot sniff test

TBD

## UEFI sniff test

TBD

## Capsule update

TBD

# EBBR Compliance

TBD

## SCT

```
|Dropped:|0|
|Failure:|1|
|Ignored:|50|
|Known Acs Limitation:|1|
|Known U-Boot Limitation:|3|
|Pass:|10464|
|Skipped:|1|
|Warning:|0|

```

TBD

## Manual SCT tests

TBD

## FWTS

```
7 passed, 0 failed, 0 warning, 1 aborted, 55 skipped, 0 info only.

Test Failure Summary
================================================================================

Critical failures: NONE

High failures: NONE

Medium failures: NONE

Low failures: NONE

Other failures: NONE

Test           |Pass |Fail |Abort|Warn |Skip |Info |
---------------+-----+-----+-----+-----+-----+-----+
esrt           |     |     |    1|     |     |     |
uefibootpath   |     |     |     |     |    1|     |
uefirtmisc     |    1|     |     |     |    8|     |
uefirttime     |    4|     |     |     |   35|     |
uefirtvariable |    2|     |     |     |   10|     |
uefivarinfo    |     |     |     |     |    1|     |
---------------+-----+-----+-----+-----+-----+-----+
Total:         |    7|    0|    1|    0|   55|    0|
---------------+-----+-----+-----+-----+-----+-----+

```

TBD

# BSA Compliance (informative)

TBD

## Bsa.efi (informative)

```
     ------------------------------------------------------- 
     Total Tests run  =   52  Tests Passed  =   34  Tests Failed =    3
     ------------------------------------------------------- 

```

TBD

## Linux bsa (informative)

TBD

# OS Installs

TBD
