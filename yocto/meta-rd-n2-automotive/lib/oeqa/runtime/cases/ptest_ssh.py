# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.core.decorator.depends import OETestDepends
from oeqa.runtime.decorator.package import OEHasPackage


class PtestRunnerTest(OERuntimeTestCase):
    @OETestDepends(['safety_island.SafetyIslandTest.test_hipc'])
    @OEHasPackage(['ptest-runner'])
    def test_ptestrunner(self):
        self.logger.info('Starting ptest-runner')
        status, output = self.target.run('ptest-runner')
        self.assertEqual(status, 0)
