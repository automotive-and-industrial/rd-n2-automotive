# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.core.decorator.depends import OETestDepends


class LinuxLoginTest(OERuntimeTestCase):
    @OETestDepends(['linuxboot.LinuxBootTest.test_linux_boot'])
    def test_linux_login(self):
        console = self.target.DEFAULT_CONSOLE

        # Login
        self.target.sendline(console, 'root')
        self.target.expect(console, r'root@.*:~#', timeout=300)
