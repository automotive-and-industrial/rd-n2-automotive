# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

from oeqa.runtime.case import OERuntimeTestCase


class ScpTest(OERuntimeTestCase):
    console = 'scp'

    def test_normal_boot(self):
        self.target.expect(self.console,
                           r'\[FWK\] Module initialization complete!',
                           timeout=30)
        self.assertNotIn(b'[ERROR]', self.target.before(self.console))
