# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.core.decorator.depends import OETestDepends
from oeqa.runtime.decorator.package import OEHasPackage


class DomUPtestRunnerTest(OERuntimeTestCase):
    @OETestDepends(['linuxlogin.LinuxLoginTest.test_linux_login'])
    @OEHasPackage(['ptest-runner'])
    def test_ptestrunner(self):
        console = self.target.DEFAULT_CONSOLE
        domu_prompt = r'root@generic-arm64:~#'
        dom0_prompt = r'root@fvp-rd-n2-automotive:~#'

        # Enter Xen console
        self.target.sendline(console, 'xl console domU')
        self.target.expect(console, r'generic-arm64 login:', timeout=300)
        self.target.sendline(console, 'root')
        self.target.expect(console, domu_prompt)

        # Run ptest-runner
        self.target.sendline(console, 'ptest-runner')
        self.target.expect(console, r'START: ptest-runner')
        self.logger.info('ptest-runner started')
        self.target.expect(console, r'TOTAL: (\d+) FAIL: (\d+)',
                           timeout=10*60)

        matches = self.target.match(console)
        self.assertGreater(int(matches[1]), 0,
                           msg='No tests have run')
        self.assertEqual(int(matches[2]), 0,
                         msg='At least one test has failed')
        self.target.expect(console, domu_prompt)

        # Return to Dom0
        self.target.sendline(console, '\x04')  # Log out
        self.target.expect(console, r'generic-arm64 login:', timeout=30)
        self.target.sendline(console, '\x1d')  # Telnet escape key
        self.target.expect(console, r'telnet>')
        self.target.sendline(console, r'send esc')
        self.target.expect(console, dom0_prompt)
