# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

from oeqa.runtime.case import OERuntimeTestCase


class RssTest(OERuntimeTestCase):
    console = 'rss'

    def test_normal_boot(self):
        self.target.expect(self.console,
                           r'Enabling non-secure core\.\.\.',
                           timeout=30)
        self.assertNotIn(b'[ERR]', self.target.before(self.console))
