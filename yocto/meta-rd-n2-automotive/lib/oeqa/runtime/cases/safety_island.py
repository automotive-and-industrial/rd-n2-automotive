# Copyright (c) 2022-2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

import re

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.core.decorator.depends import OETestDepends
from oeqa.runtime.decorator.package import OEHasPackage


class SafetyIslandTest(OERuntimeTestCase):
    linux_console = 'default'
    linux_prompt = r'root@.*:~#'
    si_console = 'safety_island_c0'
    si_prompt = r'uart:~\$ '
    domu_prompt = r'root@generic-arm64:~#'

    def setUp(self):
        super().setUp()
        if 'xen' in self.td.get('DISTRO_FEATURES').split():
            self.target.sendline(self.linux_console, 'xl console domU')
            self.target.sendline(self.linux_console)
            self.target.expect(
                self.linux_console, r'generic-arm64 login:', timeout=300)
            self.target.sendline(self.linux_console, 'root')
            self.target.expect(self.linux_console, self.domu_prompt,
                               timeout=30)

    def tearDown(self):
        super().tearDown()
        if 'xen' in self.td.get('DISTRO_FEATURES').split():
            # Return to Dom0
            self.target.sendline(self.linux_console, '\x04')  # Log out
            self.target.expect(self.linux_console, r'generic-arm64 login:',
                               timeout=30)
            self.target.sendline(self.linux_console, '\x1d')  # Telnet escape
            self.target.expect(self.linux_console, r'telnet>', timeout=30)
            self.target.sendline(self.linux_console, r'send esc')
            self.target.expect(self.linux_console, self.linux_prompt,
                               timeout=30)

    @OETestDepends(['linuxlogin.LinuxLoginTest.test_linux_login'])
    def test_ping(self):
        self.target.expect(self.si_console, r'Booting Zephyr OS build zephyr-',
                           timeout=30)
        self.target.expect(self.si_console, self.si_prompt, timeout=30)

        self.target.sendline(self.si_console, 'net ping 192.168.1.1 -c 10')
        for _ in range(0, 10):
            self.target.expect(self.si_console,
                               r'bytes from 192\.168\.1\.1 to 192\.168\.1\.2',
                               timeout=10)
        self.target.expect(self.si_console, self.si_prompt, timeout=10)

        self.target.sendline(self.linux_console, 'ping 192.168.1.2 -c 10')
        for _ in range(0, 10):
            self.target.expect(self.linux_console,
                               r'bytes from 192\.168\.1\.2',
                               timeout=10)
        self.target.expect(self.linux_console, self.linux_prompt, timeout=10)

    @OETestDepends(['safety_island.SafetyIslandTest.test_ping'])
    @OEHasPackage(['iperf'])
    def test_hipc(self):
        """
        In hipc test case, since the throughput of zperf on FVP depends
        on host performance, we only check the minimum number of
        transferred bytes(100K) to guarantee the zperf test is OK, but
        not checking the maximum throughput on the specific platform.
        """
        def check_error_messages():
            # A list of error messages that are permitted to occur in either
            # iperf or zperf
            allowed_messages = [
                b'net_tcp: context->tcp == NULL',
            ]

            matches = re.findall(br'(?:ERROR|WARN(ING)?): (.*)' b'\r\n',
                                 self.target.before(self.linux_console))
            self.assertTrue(
                all(match in allowed_messages for match in matches))

            matches = re.findall(br'<(?:err|wrn)> (.*)' b'\r\n',
                                 self.target.before(self.linux_console))
            self.assertTrue(
                all(match in allowed_messages for match in matches))

        test_duration = int(self.td.get('HIPC_PER_TEST_DURATION', 5))

        # Zephyr as UDP server
        self.target.sendline(self.si_console, 'zperf udp download')
        self.target.expect(self.si_console, 'UDP server started on port 5001',
                           timeout=30)
        self.target.sendline(
            self.linux_console,
            f'iperf -u -c 192.168.1.2 -t {test_duration} -b 100K -l 1438')
        self.target.expect(self.linux_console, self.linux_prompt,
                           timeout=100 * test_duration)
        self.target.expect(self.si_console, r'received packets:\s*(\d+)'
                           '\r\n',
                           timeout=10)
        self.assertGreater(int(self.target.match(self.si_console)[1]), 10)
        self.target.expect(self.si_console,
                           r'nb packets lost:\s*0' '\r\n',
                           timeout=10)
        self.target.expect(self.si_console,
                           r'nb packets outorder:\s*0' '\r\n',
                           timeout=10)
        check_error_messages()

        # Zephyr as TCP server
        self.target.sendline(self.si_console, 'zperf tcp download')
        self.target.expect(self.si_console, 'TCP server started on port 5001',
                           timeout=30)
        self.target.sendline(self.linux_console,
                             f'iperf -c 192.168.1.2 -t {test_duration}')
        self.target.expect(self.linux_console, self.linux_prompt,
                           timeout=100 * test_duration)
        check_error_messages()

        # Zephyr as UDP client
        self.target.sendline(self.linux_console, 'iperf -u -s -P 1')
        self.target.expect(self.linux_console,
                           'Server listening on UDP port 5001')
        # zperf udp upload <dest ip> <dest port> <duration> <packet size>
        # <bandwidth>
        self.target.sendline(
            self.si_console,
            f'zperf udp upload 192.168.1.1 5001 {test_duration} 1k 100K')
        self.target.expect(self.si_console, r'Num packets:\s*(\d+)\s',
                           timeout=100 * test_duration)
        self.assertGreater(int(self.target.match(self.si_console)[1]), 10)
        self.target.expect(self.si_console,
                           r'Num packets out order:\s*0' '\r\n',
                           timeout=100)
        self.target.expect(self.si_console,
                           r'Num packets lost:\s*0' '\r\n',
                           timeout=100)
        self.target.expect(self.si_console, self.si_prompt, timeout=15)
        self.target.expect(self.linux_console, self.linux_prompt, timeout=10)
        check_error_messages()

        # Zephyr as TCP client
        self.target.sendline(self.linux_console, 'iperf -s -P 1')
        self.target.expect(self.linux_console,
                           'Server listening on TCP port 5001')
        # zperf tcp upload <dest ip> <dest port> <duration> <packet size>
        self.target.sendline(
            self.si_console,
            f'zperf tcp upload 192.168.1.1 5001 {test_duration} 1k')
        self.target.expect(self.si_console, r'Num packets:\s*(\d+)' '\r\n',
                           timeout=100 * test_duration)
        self.assertGreater(int(self.target.match(self.si_console)[1]), 10)
        self.target.expect(self.si_console,
                           r'Num errors:\s*0 \(retry or fail\)',
                           timeout=100)
        self.target.expect(self.si_console, self.si_prompt, timeout=150)
        self.target.expect(self.linux_console, self.linux_prompt, timeout=10)
        check_error_messages()
