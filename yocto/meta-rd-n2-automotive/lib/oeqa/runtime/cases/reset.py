# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.core.decorator.depends import OETestDepends


class ResetTest(OERuntimeTestCase):
    @OETestDepends(['linuxlogin.LinuxLoginTest.test_linux_login'])
    def test_linux_login(self):
        console = self.target.DEFAULT_CONSOLE

        # Reboot from Linux
        self.target.sendline(console, 'reboot')

        # Reset from U-Boot
        for _ in range(0, 5):
            self.target.expect(console, 'Hit any key to stop autoboot:',
                               timeout=5*60)
            self.target.sendline(console)
            self.target.expect(console, 'Arm#', timeout=5*60)
            self.target.sendline(console, 'reset')

        # Return to Linux prompt
        self.target.expect(console, r"login\:", timeout=30*60)
        self.target.sendline(console, 'root')
        self.target.expect(console, r'root@.*:~#', timeout=300)
