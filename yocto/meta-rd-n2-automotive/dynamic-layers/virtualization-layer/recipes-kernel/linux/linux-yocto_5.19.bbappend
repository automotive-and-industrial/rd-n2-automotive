# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

MACHINE_VIRTUALIZATION_LINUX_YOCTO_REQUIRE ?= ""
MACHINE_VIRTUALIZATION_LINUX_YOCTO_REQUIRE:generic-arm64 = \
    "recipes-kernel/linux/linux-yocto-hipc.inc"
require ${MACHINE_VIRTUALIZATION_LINUX_YOCTO_REQUIRE}
