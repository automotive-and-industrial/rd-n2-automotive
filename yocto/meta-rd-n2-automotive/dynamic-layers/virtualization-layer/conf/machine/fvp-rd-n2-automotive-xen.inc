# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Additional configuration for the fvp-rd-n2-automotive with the "xen"
# DISTRO_FEATURE

# Xen is booted using the "chainloader" command provided by the "chain" module
GRUB_BUILDIN:append = " chain"

# Remove SDL from Xen's PACKAGECONFIG, which adds many dependencies that are
# not needed
PACKAGECONFIG:remove:pn-xen = "sdl"
PACKAGECONFIG:remove:pn-xen-tools = "sdl"

# QEMU is only used to read qcow2 images, so remove unneeded items from its
# PACKAGECONFIG
PACKAGECONFIG:remove:pn-qemu = "sdl alsa virglrenderer epoxy"

# Xen uses console hvc0
SERIAL_CONSOLES = "115200;hvc0"

# Prepend the local wic directory to the wks search path
WKS_SEARCH_PATH:prepend := "${RD_N2_AUTOMOTIVE_VIRTUALIZATION_DYNAMIC_DIR}/wic:"

# Set up the tests
TEST_SUITES:append = " ptest_domu"
TEST_FVP_LINUX_BOOT_TIMEOUT="${@40*60}"

KERNEL_FEATURES:append = "features/xen/xen.scc"
