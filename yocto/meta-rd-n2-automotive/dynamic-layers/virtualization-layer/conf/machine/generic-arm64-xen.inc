# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Install the necesasry modules for mailbox testing
MACHINE_ESSENTIAL_EXTRA_RDEPENDS += "\
    kernel-module-arm-mhuv2 \
    armv8r64-remoteproc-mod \
    rpmsg-net-mod \
    "
