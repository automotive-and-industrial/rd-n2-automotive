From 4ed740ce4db990a67a6a4fdfd56f4646513abf8f Mon Sep 17 00:00:00 2001
From: Henry Wang <Henry.Wang@arm.com>
Date: Thu, 3 Mar 2022 08:22:30 +0000
Subject: [PATCH 20/36] xen/arm: Set domain default PARTID and PMG

System register MPAM0_EL1 and MPAM1_EL1 hold information (PARTID and
PMG) to generate MPAM labels for memory requests at EL0 and EL1. This
register is context-switched when switching vCPUs.

Since if Xen needs to partition the resource of a specific VM, it
needs to limit the VM both in EL1 and EL0. This commit sets the default
PARTID and PMG to the MPAM0_EL1 and MPAM1_EL1 registers of each vCPU at
the domain creation stage. The VMID of the domain is used as the PARTID.
As PMG is not in scope for now, this commit sets all default PMGs to 0.

Signed-off-by: Henry Wang <Henry.Wang@arm.com>
Issue-Id: SCM-3395
Signed-off-by: Peter Hoyes <Peter.Hoyes@arm.com>
Upstream-Status: Pending
---
 xen/arch/arm/domain_build.c |  6 ++++++
 xen/arch/arm/mpam.c         | 30 ++++++++++++++++++++++++++++--
 xen/include/asm-arm/mpam.h  |  8 ++++++++
 3 files changed, 42 insertions(+), 2 deletions(-)

diff --git a/xen/arch/arm/domain_build.c b/xen/arch/arm/domain_build.c
index d02bacbcd1..ed9559d005 100644
--- a/xen/arch/arm/domain_build.c
+++ b/xen/arch/arm/domain_build.c
@@ -20,6 +20,7 @@
 #include <acpi/actables.h>
 #include <asm/device.h>
 #include <asm/kernel.h>
+#include <asm/mpam.h>
 #include <asm/setup.h>
 #include <asm/tee/tee.h>
 #include <asm/platform.h>
@@ -2912,6 +2913,11 @@ static int __init construct_domain(struct domain *d, struct kernel_info *kinfo)
 
     domain_update_node_affinity(d);
 
+#ifdef CONFIG_MPAM
+    if ( mpam_enabled )
+        mpam_update_domain(d);
+#endif
+
     v->is_initialised = 1;
     clear_bit(_VPF_down, &v->pause_flags);
 
diff --git a/xen/arch/arm/mpam.c b/xen/arch/arm/mpam.c
index a7f20c8aca..42c95b11d7 100644
--- a/xen/arch/arm/mpam.c
+++ b/xen/arch/arm/mpam.c
@@ -28,6 +28,7 @@
 #include <asm/mpam.h>
 #include <asm/smp.h>
 
+bool __read_mostly mpam_enabled = false;
 /*
  * mpam_list_lock protects the global lists when writing.
  * Once the MPAM is enabled these lists are read-only.
@@ -52,8 +53,6 @@ static unsigned int mpam_fw_num_msc;
 static uint16_t mpam_partid_max;
 static uint8_t mpam_pmg_max;
 
-static bool __read_mostly mpam_enabled = false;
-
 static struct dt_device_node *
 __init dt_find_next_cache_node(const struct dt_device_node *np)
 {
@@ -1190,6 +1189,28 @@ static int __init mpam_enable(void)
     return 0;
 }
 
+static void __init mpam_set_domain_partid_pmg(struct domain *d,
+                                              uint16_t partid_d,
+                                              uint16_t partid_i,
+                                              uint8_t pmg_d,
+                                              uint8_t pmg_i)
+{
+    struct vcpu *vcpu;
+
+    for_each_vcpu( d, vcpu )
+    {
+        uint64_t regval;
+
+        regval = FIELD_PREP(MPAM_SYSREG_PARTID_D, partid_d);
+        regval |= FIELD_PREP(MPAM_SYSREG_PARTID_I, partid_i);
+        regval |= FIELD_PREP(MPAM_SYSREG_PMG_D, pmg_d);
+        regval |= FIELD_PREP(MPAM_SYSREG_PMG_I, pmg_i);
+
+        vcpu->arch.mpam0_el1 = regval;
+        vcpu->arch.mpam1_el1 = regval;
+    }
+}
+
 static int __init mpam_msc_probe(void)
 {
     unsigned int num_msc = 0;
@@ -1296,6 +1317,11 @@ void mpam_restore_state(struct vcpu *n)
     WRITE_SYSREG(n->arch.mpam1_el1, MPAM1_EL1);
 }
 
+void mpam_update_domain(struct domain *d)
+{
+    mpam_set_domain_partid_pmg(d, d->arch.p2m.vmid, d->arch.p2m.vmid, 0, 0);
+}
+
 /*
  * Local variables:
  * mode: C
diff --git a/xen/include/asm-arm/mpam.h b/xen/include/asm-arm/mpam.h
index 185e99ea10..b03f9a8e75 100644
--- a/xen/include/asm-arm/mpam.h
+++ b/xen/include/asm-arm/mpam.h
@@ -257,6 +257,11 @@
 #define MSMON_CAPT_EVNT_NOW    BIT(0, UL)
 
 /* Field descriptions for MPAM system registers: */
+#define MPAM_SYSREG_PMG_D      GENMASK(47, 40)
+#define MPAM_SYSREG_PMG_I      GENMASK(39, 32)
+#define MPAM_SYSREG_PARTID_D   GENMASK(31, 16)
+#define MPAM_SYSREG_PARTID_I   GENMASK(15, 0)
+
 #define MPAMIDR_PMG_MAX        GENMASK(40, 32)
 #define MPAMIDR_PARTID_MAX     GENMASK(15, 0)
 
@@ -407,10 +412,13 @@ struct mpam_msc_ris
     struct mpam_component *comp;
 };
 
+extern bool mpam_enabled;
+
 extern void mpam_postsmp_setup(void);
 extern void mpam_presmp_setup(void);
 extern void mpam_restore_state(struct vcpu *n);
 extern void mpam_save_state(struct vcpu *n);
+extern void mpam_update_domain(struct domain *d);
 extern int mpam_discovery_cpu_online(unsigned int cpu);
 
 #endif /* __ARM_MPAM_H__ */
-- 
2.25.1

