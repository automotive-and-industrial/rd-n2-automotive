From 82349756585416eae37fbfe365c768d149959b8e Mon Sep 17 00:00:00 2001
From: Henry Wang <Henry.Wang@arm.com>
Date: Wed, 8 Jun 2022 08:11:00 +0000
Subject: [PATCH 25/36] tools/xl: Expose xl-psr to Arm64

xl-psr [1] is a set of xl interfaces that used by Intel Platform Shared
Resource Monitoring/Control technologies. These interfaces allows a
Hypervisor/VMM to determine the usage of cache (currently only L3 cache
supported) and memory bandwidth by applications/VMs running on the
platform. Also xl-psr provides commands to configure and use the monitor
to tracks the cache or memory bandwidth utilization according to the RMID
and reports monitored data.

Since the MPAM support of Xen would need exact same interfaces, this
commit exposes the xl-psr to Arm64. Further commits will enable the
commands/interfaces of xl-psr to reuse xl-psr for MPAM domain
configurations and monitor.

[1] https://xenbits.xen.org/docs/unstable/misc/xl-psr.html

Signed-off-by: Henry Wang <Henry.Wang@arm.com>
Issue-Id: SCM-2795
Signed-off-by: Peter Hoyes <Peter.Hoyes@arm.com>
Upstream-Status: Pending
---
 tools/include/libxl.h     | 4 ++--
 tools/include/xenctrl.h   | 5 ++++-
 tools/libs/ctrl/Makefile  | 1 +
 tools/libs/light/Makefile | 1 +
 tools/xl/Makefile         | 1 +
 tools/xl/xl.h             | 2 +-
 tools/xl/xl_cmdtable.c    | 2 +-
 7 files changed, 11 insertions(+), 5 deletions(-)

diff --git a/tools/include/libxl.h b/tools/include/libxl.h
index 2bbbd21f0b..3fbd1a8563 100644
--- a/tools/include/libxl.h
+++ b/tools/include/libxl.h
@@ -1164,7 +1164,7 @@ typedef uint8_t libxl_mac[6];
 #define LIBXL_MAC_BYTES(mac) mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]
 void libxl_mac_copy(libxl_ctx *ctx, libxl_mac *dst, const libxl_mac *src);
 
-#if defined(__i386__) || defined(__x86_64__)
+#if defined(__i386__) || defined(__x86_64__) || defined(__aarch64__)
 /*
  * LIBXL_HAVE_PSR_CMT
  *
@@ -2713,7 +2713,7 @@ bool libxl_ms_vm_genid_is_zero(const libxl_ms_vm_genid *id);
 void libxl_ms_vm_genid_copy(libxl_ctx *ctx, libxl_ms_vm_genid *dst,
                             const libxl_ms_vm_genid *src);
 
-#if defined(__i386__) || defined(__x86_64__)
+#if defined(__i386__) || defined(__x86_64__) || defined(__aarch64__)
 int libxl_psr_cmt_attach(libxl_ctx *ctx, uint32_t domid);
 int libxl_psr_cmt_detach(libxl_ctx *ctx, uint32_t domid);
 int libxl_psr_cmt_domain_attached(libxl_ctx *ctx, uint32_t domid);
diff --git a/tools/include/xenctrl.h b/tools/include/xenctrl.h
index 893ae39e4a..15603b610b 100644
--- a/tools/include/xenctrl.h
+++ b/tools/include/xenctrl.h
@@ -2466,7 +2466,7 @@ struct xc_resource_op {
 typedef struct xc_resource_op xc_resource_op_t;
 int xc_resource_op(xc_interface *xch, uint32_t nr_ops, xc_resource_op_t *ops);
 
-#if defined(__i386__) || defined(__x86_64__)
+#if defined(__i386__) || defined(__x86_64__) || defined(__aarch64__)
 enum xc_psr_cmt_type {
     XC_PSR_CMT_L3_OCCUPANCY,
     XC_PSR_CMT_TOTAL_MEM_COUNT,
@@ -2529,6 +2529,9 @@ int xc_psr_get_domain_data(xc_interface *xch, uint32_t domid,
 int xc_psr_get_hw_info(xc_interface *xch, uint32_t socket,
                        xc_psr_feat_type type, xc_psr_hw_info *hw_info);
 
+#endif
+
+#if defined(__i386__) || defined(__x86_64__)
 typedef struct xc_cpu_policy xc_cpu_policy_t;
 
 /* Create and free a xc_cpu_policy object. */
diff --git a/tools/libs/ctrl/Makefile b/tools/libs/ctrl/Makefile
index 6ff5918798..23668f4f38 100644
--- a/tools/libs/ctrl/Makefile
+++ b/tools/libs/ctrl/Makefile
@@ -28,6 +28,7 @@ SRCS-y       += xc_foreign_memory.c
 SRCS-y       += xc_kexec.c
 SRCS-y       += xc_resource.c
 SRCS-$(CONFIG_X86) += xc_psr.c
+SRCS-$(CONFIG_ARM_64) += xc_psr.c
 SRCS-$(CONFIG_X86) += xc_pagetab.c
 SRCS-$(CONFIG_Linux) += xc_linux.c
 SRCS-$(CONFIG_FreeBSD) += xc_freebsd.c
diff --git a/tools/libs/light/Makefile b/tools/libs/light/Makefile
index dd43f37b4c..a483a60972 100644
--- a/tools/libs/light/Makefile
+++ b/tools/libs/light/Makefile
@@ -51,6 +51,7 @@ CFLAGS-$(CONFIG_X86) += -DCONFIG_PCI_SUPP_LEGACY_IRQ
 SRCS-$(CONFIG_X86) += libxl_cpuid.c
 SRCS-$(CONFIG_X86) += libxl_x86.c
 SRCS-$(CONFIG_X86) += libxl_psr.c
+SRCS-$(CONFIG_ARM_64) += libxl_psr.c
 SRCS-$(CONFIG_X86) += libxl_x86_acpi.c
 SRCS-$(CONFIG_ARM) += libxl_nocpuid.c
 SRCS-$(CONFIG_ARM) += libxl_arm.c
diff --git a/tools/xl/Makefile b/tools/xl/Makefile
index 656b21c7da..df81ac2f71 100644
--- a/tools/xl/Makefile
+++ b/tools/xl/Makefile
@@ -17,6 +17,7 @@ CFLAGS_XL += $(CFLAGS_libxenutil)
 CFLAGS_XL += -Wshadow
 
 XL_OBJS-$(CONFIG_X86) = xl_psr.o
+XL_OBJS-$(CONFIG_ARM_64) = xl_psr.o
 XL_OBJS = xl.o xl_cmdtable.o xl_sxp.o xl_utils.o $(XL_OBJS-y)
 XL_OBJS += xl_parse.o xl_cpupool.o xl_flask.o
 XL_OBJS += xl_vtpm.o xl_block.o xl_nic.o xl_usb.o
diff --git a/tools/xl/xl.h b/tools/xl/xl.h
index cf12c79a9b..2ed76320d9 100644
--- a/tools/xl/xl.h
+++ b/tools/xl/xl.h
@@ -203,7 +203,7 @@ int main_loadpolicy(int argc, char **argv);
 int main_remus(int argc, char **argv);
 #endif
 int main_devd(int argc, char **argv);
-#if defined(__i386__) || defined(__x86_64__)
+#if defined(__i386__) || defined(__x86_64__) || defined(__aarch64__)
 int main_psr_hwinfo(int argc, char **argv);
 int main_psr_cmt_attach(int argc, char **argv);
 int main_psr_cmt_detach(int argc, char **argv);
diff --git a/tools/xl/xl_cmdtable.c b/tools/xl/xl_cmdtable.c
index 661323d488..8032b8845a 100644
--- a/tools/xl/xl_cmdtable.c
+++ b/tools/xl/xl_cmdtable.c
@@ -545,7 +545,7 @@ const struct cmd_spec cmd_table[] = {
       "-F                      Run in the foreground.\n"
       "-p, --pidfile [FILE]    Write PID to pidfile when daemonizing.",
     },
-#if defined(__i386__) || defined(__x86_64__)
+#if defined(__i386__) || defined(__x86_64__) || defined(__aarch64__)
     { "psr-hwinfo",
       &main_psr_hwinfo, 0, 1,
       "Show hardware information for Platform Shared Resource",
-- 
2.25.1

