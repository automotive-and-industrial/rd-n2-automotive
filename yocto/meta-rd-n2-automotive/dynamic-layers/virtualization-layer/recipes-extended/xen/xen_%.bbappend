#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

MACHINE_XEN_REQUIRE ?= ""
MACHINE_XEN_REQUIRE:fvp-rd-n2-automotive = "xen-rd-n2-automotive-fvp.inc"
require ${MACHINE_XEN_REQUIRE}
