#
# Copyright (c) 2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

do_install:append(){
    # We want to make sure that the domain will be "dead" when "xl shutdown" is called
    sed -i 's#XENDOMAINS_SHUTDOWN="--wait"#XENDOMAINS_SHUTDOWN="--wait --wait"#' ${D}${sysconfdir}/default/xendomains
}

