#
# Copyright (c) 2022-2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

MACHINE_XEN_TOOLS_REQUIRE ?= ""
MACHINE_XEN_TOOLS_REQUIRE:fvp-rd-n2-automotive = "xen-rd-n2-automotive-fvp.inc xen-tools-rd-n2-automotive-fvp.inc"
require ${MACHINE_XEN_TOOLS_REQUIRE}
