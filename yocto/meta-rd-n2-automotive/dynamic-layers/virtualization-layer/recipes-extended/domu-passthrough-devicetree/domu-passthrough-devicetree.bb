# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Passthrough device tree for a DomU on the FVP_RD_N2_Automotive"
DESCRIPTION = "A recipe to compile the device tree to pass through devices to \
a DomU running on the FVP_RD_N2_Automotive."

inherit devicetree
PROVIDES:remove = "virtual/dtb"

COMPATIBLE_MACHINE = "fvp-rd-n2-automotive"
FILESEXTRAPATHS:prepend := "\
${RD_N2_AUTOMOTIVE_REPO_DIRECTORY}/components/primary_compute/devicetree:"
SRC_URI = "file://fvp-rd-n2-automotive-passthrough.dts"

do_install() {
    install -Dm 0644 ${B}/fvp-rd-n2-automotive-passthrough.dtb \
        ${D}${datadir}/doms/fvp-rd-n2-automotive-passthrough.dtb
}
FILES:${PN} = "${datadir}/doms"
