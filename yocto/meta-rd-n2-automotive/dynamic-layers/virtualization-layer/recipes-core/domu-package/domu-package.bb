#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Xen DomU package"
DESCRIPTION = "A recipe to bundle a DomU image and configuration as a package\
which can be installed in Dom0"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "\
    file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
    "

inherit features_check
REQUIRED_DISTRO_FEATURES += "xen"

RDEPENDS:${PN} = "domu-passthrough-devicetree"

do_patch[noexec] = "1"
do_configure[noexec] = "1"
do_compile[noexec] = "1"

DOMU_KERNEL_IMG = "Image-${DOMU_MACHINE}.bin"
DOMU_DISK_IMG = "domu-image-${DOMU_MACHINE}.wic.qcow2"
DOMU_DIR = "${datadir}/doms"
XEN_AUTO_DIR = "${sysconfdir}/xen/auto"
DOMU_MEMORY_SIZE ?= "1024"
DOMU_VCPUS ?= "4"

do_install() {
    install -d ${D}${XEN_AUTO_DIR}
     cat <<EOF >> ${D}${XEN_AUTO_DIR}/domu.cfg
name = "domU"
memory = ${DOMU_MEMORY_SIZE}
vcpus = ${DOMU_VCPUS}
extra = " earlyprintk=xenboot console=hvc0 rw"
root = "/dev/xvda1"
kernel = "${DOMU_DIR}/${DOMU_KERNEL_IMG}"
disk = ['format=qcow2, vdev=xvda, access=rw, backendtype=qdisk, \
target=${DOMU_DIR}/${DOMU_DISK_IMG}']
vif = ['script=vif-bridge,bridge=xenbr0']
irqs = [ 67 ]
iomem = [ "0x2aaa0,1@0x22000", "0x2aab0,1@0x22010", "0x8080000,1", "0x8080001,1", "0x8080002,1", "0x8080003,200" ]
device_tree = "${DOMU_DIR}/fvp-rd-n2-automotive-passthrough.dtb"
EOF

    install -d ${D}${DOMU_DIR}

    install -Dm 0644 ${DOMU_TMPDIR}/deploy/images/${DOMU_MACHINE}/${DOMU_KERNEL_IMG} \
        ${D}${DOMU_DIR}/${DOMU_KERNEL_IMG}

    install -Dm 0644 ${DOMU_TMPDIR}/deploy/images/${DOMU_MACHINE}/${DOMU_DISK_IMG} \
        ${D}${DOMU_DIR}/${DOMU_DISK_IMG}
}
do_install[mcdepends] += "mc::domu:domu-image:do_image_complete"

FILES:${PN} = "${DOMU_DIR} ${XEN_AUTO_DIR}"
