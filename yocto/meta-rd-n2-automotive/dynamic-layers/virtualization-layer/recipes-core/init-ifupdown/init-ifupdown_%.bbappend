# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:fvp-rd-n2-automotive := \
                        "${@bb.utils.contains('DISTRO_FEATURES', \
                         'xen', \
                         '${THISDIR}/files:', \
                         '', d)}"

# Dom0 xen bridge network configuration
SRC_URI:append:fvp-rd-n2-automotive = \
    " file://interfaces.d/1001-xen-bridge"

# DomU Safety Island virtual network configuration
SRC_URI:append:generic-arm64 = \
    " file://interfaces.d/0002-safety-island"
