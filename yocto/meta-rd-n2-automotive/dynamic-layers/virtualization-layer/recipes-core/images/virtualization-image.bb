#
# Copyright (c) 2022-2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Virtualization image"
DESCRIPTION = "An image recipe, based on core-image, which additionally \
includes xen and xen-cfg in the boot partition"

require recipes-core/images/baremetal-image.bb

GRUB_CFG_FILE = \
"${RD_N2_AUTOMOTIVE_VIRTUALIZATION_DYNAMIC_DIR}/wic/virtualization-grub.cfg"

IMAGE_EFI_BOOT_FILES:append = " xen-${MACHINE}.efi;xen.efi xen.cfg"
do_image_wic[depends] += "xen:do_deploy xen-cfg:do_deploy"

EXTRA_IMAGEDEPENDS += "xen xen-cfg"

IMAGE_OVERHEAD_FACTOR = "1.5"

IMAGE_INSTALL += "\
    xen-tools \
    domu-package \
    kernel-module-xen-netback \
    kernel-module-xen-gntalloc \
    kernel-module-xen-gntdev \
"

CONFLICT_DISTRO_FEATURES:remove = "xen"
REQUIRED_DISTRO_FEATURES = "xen"
