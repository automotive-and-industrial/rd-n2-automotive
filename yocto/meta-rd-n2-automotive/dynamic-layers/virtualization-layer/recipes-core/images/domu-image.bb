#
# Copyright (c) 2022-2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Xen DomU image"
DESCRIPTION = "An image recipe, based on core-image, which builds a qcow2 disk\
image for a DomU without a boot partition"

inherit core-image

IMAGE_OVERHEAD_FACTOR = "1.5"
IMAGE_FSTYPES = "wic.qcow2"
IMAGE_INSTALL = "packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL} \
    system-tests-ptest iperf"
IMAGE_LINGUAS = ""

WKS_FILE = "domu-disk.wks"

inherit features_check
REQUIRED_DISTRO_FEATURES = "xen"
