#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "SystemReady IR ACS"
DESCRIPTION = "SystemReady IR Architecture Compliance Suite Pre-Built image"

# Set the LICENSE_FLAGS to "Arm-FVP-EULA" since the tests will only be able to
# run if the fvp is being installed and remove image license collation.
LICENSE_FLAGS = "Arm-FVP-EULA"
LICENSE = "Apache-2.0 & GPL-2.0-only & BSD-2-Clause-Patent & BSD-2-Clause \
           & GPL-2.0-or-later & GPL-3.0-only & LGPL-2.1-only"
LIC_FILES_CHKSUM = "\
file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10 \
file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6 \
file://${COMMON_LICENSE_DIR}/BSD-2-Clause-Patent;md5=0518d409dae93098cca8dfa932f3ab1b \
file://${COMMON_LICENSE_DIR}/BSD-2-Clause;md5=cb641bc04cda31daea161b1bc15da69f \
file://${COMMON_LICENSE_DIR}/GPL-2.0-or-later;md5=fed54355545ffd980b814dab4a3b312c \
file://${COMMON_LICENSE_DIR}/GPL-3.0-only;md5=c79ff39f19dfec6d293b95dea7b07891 \
file://${COMMON_LICENSE_DIR}/LGPL-2.1-only;md5=1a6d268fd218675ffea8be556788b780 \
"
IMAGE_CLASSES:remove = "license_image"

COMPATIBLE_MACHINE = "fvp-*"

TEST_SUITES = "systemready_ir_acs"

PV ?= "1.0"
PV_DATE ?= "21.09"
FULL_PV ?= "v${PV_DATE}_${PV}"
SYSTEMREADY_IR_ACS_BRANCH ?= "main"
IMAGE_FILENAME = "ir_acs_live_image.img"
SRC_URI = "https://github.com/ARM-software/arm-systemready/raw/${SYSTEMREADY_IR_ACS_BRANCH}/IR/prebuilt_images/${FULL_PV}/${IMAGE_FILENAME}.xz"
SRC_URI[sha256sum] = "1fb61867958b7744219b6126858bb6caaa5b79263fb3c1ed60b66ce8529e3fbe"

inherit systemready-acs
