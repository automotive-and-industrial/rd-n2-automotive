#
# Copyright (c) 2022-2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "EDK2 Test Parser"
DESCRIPTION = "EDK2 Test Parser for parsing the results of UEFI SCT tests"
HOMEPAGE = "https://gitlab.arm.com/systemready/edk2-test-parser"

LICENSE = "BSD-2-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=c0550be4b3b9c0223efd0eaa70dc9085"

RDEPENDS:${PN} = "python3-packaging python3-pyyaml python3-jsonschema"

PV = "ir1-${SRCPV}"
SRC_URI = "git://git.gitlab.arm.com/systemready/edk2-test-parser.git;protocol=https;nobranch=1"

# The SRCREV is in the branch ir1
SRCREV  = "10f0941019394c3b0c5c669dffde8c4da71bad02"

do_install() {
    install -d ${D}/${libdir}/edk2_test_parser
    cp -r ${WORKDIR}/git/* ${D}/${libdir}/edk2_test_parser
}

BBCLASSEXTEND = "native"
