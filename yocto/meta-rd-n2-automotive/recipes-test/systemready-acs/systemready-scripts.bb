#
# Copyright (c) 2022-2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "System Ready Scripts"
DESCRIPTION = "A collection of scripts to help with SystemReady compliance."
HOMEPAGE = "https://gitlab.arm.com/systemready/systemready-scripts"

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=85b7d439a311c22626c2e3f05daf628e"

RDEPENDS:${PN} = "python3-packaging python3-pyyaml"

PV = "ir1-${SRCPV}"
SRC_URI = "git://git.gitlab.arm.com/systemready/systemready-scripts.git;protocol=https;nobranch=1"

# The SRCREV is in the branch ir1
SRCREV  = "75b51d90983812450edfe748b8f44c48de1e3e1a"

do_install() {
    install -d ${D}/${libdir}/systemready_scripts
    cp -r ${WORKDIR}/git/* ${D}/${libdir}/systemready_scripts
}

BBCLASSEXTEND = "native"
