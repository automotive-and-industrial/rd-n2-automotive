# Copyright (c) 2022-2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "System tests"
DESCRIPTION = "A set of generic system tests using BATS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit ptest

RDEPENDS:${PN} += "bats"

SRC_URI = "file://tests"
S = "${WORKDIR}/tests"
TESTS_PATH = "${libdir}/${BPN}"
COMPATIBLE_MACHINE = "(fvp-rd-n2-automotive|generic-arm64)"

# A set of tags to restrict the tests that run based on the current
# stack/machine
SYSTEM_TESTS_FILTER_TAGS_STACK:fvp-rd-n2-automotive = \
    "${@bb.utils.contains('DISTRO_FEATURES', 'xen', \
                          'virtualization dom0', 'baremetal', d)}"
SYSTEM_TESTS_FILTER_TAGS_STACK:generic-arm64 = "virtualization domu"
# An optional set of extra tags, which can be specified globally to further
# restrict which tests run
SYSTEM_TESTS_FILTER_TAGS_EXTRA ?= ""
# The full list of test tags to pass to BATS
SYSTEM_TESTS_FILTER_TAGS ?= "${SYSTEM_TESTS_FILTER_TAGS_STACK} \
${SYSTEM_TESTS_FILTER_TAGS_EXTRA}"

BATS_EXTRA_OPTIONS ?= "\
--verbose-run \
--trace \
--timing \
--print-output-on-failure \
--show-output-of-passing-tests"

do_configure[noexec] = "1"

do_compile() {
    cat <<EOF > ${WORKDIR}/run-ptest
#!/bin/sh
bats "${TESTS_PATH}/tests" --filter-tags \
"${@','.join(d.getVar('SYSTEM_TESTS_FILTER_TAGS').split())}" \
${BATS_EXTRA_OPTIONS}
EOF
}

do_install() {
    install -d ${D}${TESTS_PATH}
    cp -rf ${S} ${D}${TESTS_PATH}
}
