#!/usr/bin/env bats
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

load utils.bash

# bats file_tags=networking,baremetal,virtualization,dom0,domu

@test "Check networking devices" {
    check_devices "net" 2 "virtio_net|vif"

    # Check outbound network connections work
    run wget -O /dev/null "https://www.arm.com"
    echo "${output}"
    [ "${status}" -eq 0 ]
}
