#!/usr/bin/env bats
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# bats file_tags=smp,baremetal,virtualization,dom0,domu

enable_cpu() {
    echo "1" > "/sys/devices/system/cpu/cpu$1/online"
}

disable_cpu() {
    echo "0" > "/sys/devices/system/cpu/cpu$1/online"
}

setup_file() {
    NUM_CPUS="$(find /sys/firmware/devicetree/base/cpus/ \
        -name "cpu@*" -maxdepth 1 | wc -l)"
    export NUM_CPUS
}

teardown() {
    # Ensure all CPUs are re-enabled if a test fails
    for ((i=0; i < NUM_CPUS; i++)); do
        run enable_cpu "$i"
    done
}

@test "More than one CPU available" {
    [ "${NUM_CPUS}" -gt 1 ]
}

@test "All cores are online" {
    run grep -c "processor" /proc/cpuinfo
    [ "${lines[0]}" -eq "${NUM_CPUS}" ]
}

@test "Can stop each core individually" {
    for ((i=0; i < NUM_CPUS; i++)); do
        echo "Disabling CPU ${i}"
        run disable_cpu "$i"
        echo "${output}"
        [ "$status" -eq 0 ]

        echo "Re-enabling CPU ${i}"
        run enable_cpu "$i"
        echo "${output}"
        [ "$status" -eq 0 ]
    done
}

@test "Cannot disable all cores" {
    for ((i=0; i < NUM_CPUS - 1; i++)); do
        echo "Disabling CPU ${i}"
        run disable_cpu "$i"
        echo "${output}"
        [ "$status" -eq 0 ]
    done

    # Disabling last core should trigger an error
    run disable_cpu $((NUM_CPUS - 1))
    [ "$status" -ne 0 ]
}
