#!/usr/bin/env bash
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

check_devices() {
    local class
    local count
    local driver
    class=$1
    count=$2
    driver=$3

    # Find all the devices of $class and store in global array DEVICES
    mapfile -t DEVICES < <(find "/sys/class/$class" -type l -maxdepth 1)
    echo "devices: " "${DEVICES[@]}"

    # Assert that the number of DEVICES is greater than or equal to $count
    [ "${#DEVICES[@]}" -ge "$count" ]

    # Assert that at least one of DEVICES uses $driver
    local found
    for device in "${DEVICES[@]}"; do
        echo "device: ${device}"
        # Query the driver
        local device_driver
        device_driver=$(basename "$(readlink "${device}/device/driver")")
        echo "device_driver: ${device_driver}"
        if [[ "${device_driver}" =~ ${driver} ]]; then
            found=1
        fi
    done
    [ "${found}" ]
}
