#!/usr/bin/env bats
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

load utils.bash

# bats file_tags=watchdog,baremetal,virtualization,dom0

@test "Check watchdog devices" {
    check_devices "watchdog" 1 "sp805-wdt"
}
