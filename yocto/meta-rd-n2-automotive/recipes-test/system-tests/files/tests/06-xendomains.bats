#!/usr/bin/env bats
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# bats file_tags=xendomains,virtualization,dom0

check_domu_status() {
    run bash -c 'xl list | grep domU'
    echo "${output}"
    [ "$status" -eq "$1" ]
}

check_cpbm_value() {
    local dom
    local value
    dom=$1
    value=$2

    run bash -c "xl psr-cat-show -l0 $dom | grep $dom | awk '{print $2}'"
    echo "${output}"
    [ "$status" -eq 0 ]
    [ $((output)) -eq $((value)) ]
}

@test "Check restarting Xen domains" {
    check_domu_status 0

    echo "# Stopping Xen domains" >&3
    # Both Xen and BATS use file descriptor 3, so it is necessary to duplicate
    # the file handle
    run /etc/init.d/xendomains stop 3>&-
    echo "${output}"
    [ "$status" -eq 0 ]

    check_domu_status 1

    echo "# Restarting Xen domains" >&3
    run /etc/init.d/xendomains start 3>&-
    echo "${output}"
    [ "$status" -eq 0 ]

    check_domu_status 0
}

@test "Verify MPAM enablement" {
    # MPAM is enabled for dom0 at boot
    # Verify its status
    check_cpbm_value Domain-0 0xff

    # Enable MPAM for DomU and verify
    run xl psr-cat-set -l0 domU 0xff
    [ "$status" -eq 0 ]
    check_cpbm_value domU 0xff

    # Disable MPAM for DomU and verify
    run xl psr-cat-set -l0 domU 0x0
    [ "$status" -eq 0 ]
    check_cpbm_value domU 0
}
