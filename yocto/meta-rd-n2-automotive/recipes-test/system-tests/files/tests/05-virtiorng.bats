#!/usr/bin/env bats
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# bats file_tags=virtiorng,baremetal,virtualization,dom0

check_rng() {
    run bash -c "cat ${1} | grep ${2}"
    echo "${output}"
    [ "$status" -eq 0 ]
}

@test "Check virtio-rng devices" {

    check_rng /sys/devices/virtual/misc/hw_random/rng_available virtio_rng.0

    check_rng /sys/devices/virtual/misc/hw_random/rng_current virtio_rng.0

    run hexdump -n 32 /dev/hwrng
    echo "${output}"
    [ "$status" -eq 0 ]
}
