<!--
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
-->

# Arm Neoverse N2 Automotive Reference Design

The Arm® Neoverse™ N2 Automotive Reference Design is a Fixed Virtual Platform
(FVP) based system that introduces the concept of a high-performance Application
Processor (Primary Compute) system augmented with a Cortex®-R based Safety
Island for scenarios where additional system safety monitoring is required. The
Reference Design also includes a Runtime Security Subsystem used for secure boot
services used by the different elements.

The Arm Neoverse™ N2 Automotive Reference Software Stack is a collection of
example software compositions for running on the Arm Neoverse™ N2 Automotive
Reference Design.

## Arm Neoverse N2 Automotive Reference Software Stack Documentation

The project's documentation can be browsed at
<https://rd-n2-automotive.docs.arm.com>.

To build a local version of the documentation, you will need [Sphinx][2]
installed in your work environment. The guidance at [Installing Sphinx][3]
provides detailed information.

[2]: https://www.sphinx-doc.org/
<!-- inclusivity-exception -->
[3]: https://www.sphinx-doc.org/en/master/usage/installation.html

The following commands should be executed on a Linux machine and have been
tested on hosts running Ubuntu 20.04 LTS. These can be used to generate an HTML
version of the documentation under `public/`:

    sudo apt-get install python3-pip
    pip3 install -U -r documentation/requirements.txt
    sphinx-build -b html -a -W documentation public

To render and explore the documentation, simply open `public/index.html` in a
web browser.

## Repository License

The repository's standard licence is the MIT license, under which most of the
repository's content is provided. Exceptions to this standard license relate to
files that represent modifications to externally licensed works (for example,
patch files). These files may therefore be included in the repository under
alternative licenses in order to be compliant with the licensing requirements of
the associated external works.

License details may be found in the [local license file](license.rst), or as
part of the project documentation.

Contributions to the project should follow the same licensing arrangement.

## Contact

Please see the project documentation for the list of maintainers, as well as the
process for submitting contributions, raising issues, or receiving feedback and
support.
