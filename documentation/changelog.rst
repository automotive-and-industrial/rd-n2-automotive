..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

#########################
Changelog & Release Notes
#########################

****
v1.0
****

New Features
============

Implementation of the
:ref:`Use-Cases <introduction_use_cases_and_reference_software_stack_overview>`.

Components versions used in the Reference Stack:

.. list-table::
  :header-rows: 1

  * - Component
    - Version
    - Source
  * - Neoverse N2 Automotive Reference Design FVP (FVP_RD_N2_Automotive)
    - |FVP_RD_N2_Automotive version|
    - |FVP download|_
  * - Trusted Firmware-M (RSS)
    - |Trusted Firmware-M version| (based on |Trusted Firmware-M base version|)
    - |Trusted Firmware-M repository|_
  * - SCP-firmware
    - |SCP-Firmware version| (based on |SCP-Firmware base version|)
    - |SCP-Firmware repository|_
  * - Trusted Firmware-A
    - |Trusted Firmware-A version| (based on |Trusted Firmware-A base version|)
    - |Trusted Firmware-A repository|_
  * - U-Boot
    - |U-Boot version|
    - |U-Boot repository|_
  * - Xen
    - |Xen version|
    - |Xen repository|_
  * - Linux Kernel
    - |Linux version|
    - |Linux repository|_
  * - Zephyr
    - |Zephyr version|
    - |Zephyr repository|_


Third-party Yocto layers used to build the Reference Stack:

  .. code-block:: yaml
    :substitutions:

    URL: |meta-arm repository|
    layers: meta-arm, meta-arm-toolchain
    branch: |meta-arm branch|
    revision: |meta-arm revision|

    URL: |poky repository|
    layers: meta, meta-poky
    branch: |poky branch|
    revision: |poky revision|

    URL: |meta-openembedded repository|
    layers: meta-filesystems, meta-networking, meta-oe, meta-python
    branch: |meta-openembedded branch|
    revision: |meta-openembedded revision|

    URL: |meta-zephyr repository|
    layers: meta-zephyr-core
    branch: |meta-zephyr branch|
    revision: |meta-zephyr revision|

    URL: |meta-virtualization repository|
    layers: meta-virtualization
    branch: |meta-virtualization branch|
    revision: |meta-virtualization revision|

Changed
=======

Initial version.

.. _changelog_limitations:

Limitations
===========
 * Currently, MPAM support in Xen is available for the system level cache (SLC)
   partitioning only.
 * In the Virtualization Reference Stack Architecture, DomU MPAM settings can
   only be manipulated by xl after the DomU has been created and started.
 * The FVP only provides the programmer's view of MPAM. There is no functional
   behaviour change implemented.
 * SCP RAM Firmware is not covered by the RSS-oriented secure boot process. The
   binary of SCP RAM Firmware is preloaded in SRAM when the FVP is launched.
 * In the Virtualization Architecture, the Virtual Network connection recovery
   after restarting the DomU guest is not supported.
 * Build and runtime validation is only supported on x86_64 host machines.
 * In the HIPC, the iperf parameter "-l/--length" should be less than 1473 (IP
   and UDP overhead) in the case of Zephyr running as a UDP server since it does
   not support IP fragmentation.

Resolved and Known Issues
=========================

.. _changelog_knownissues:

Known Issues
------------
In the HIPC, some logs about packet buffer exhaustion or traffic sending failure can be observed
because of FVP performance bottleneck. Some logs and possible causes are listed
here:

 * In the zperf sample, when the packet buff pool is consumed,
   ``veth_rpmsg: Failed to allocate packet.`` will be printed.
 * FVP unfair scheduling results in asynchronism between the TCP sender and the TCP receiver.
   When the sender has sent many packets, and the receiver does not reply with any ack since it
   was not scheduled, it will cause the size of the TCP window to be too small to send any packet.
   Finally, ``Failed to send the packet (-11)`` will be logged.
 * In the zperf TCP testing case, if the application layer does not process the packet buffer
   in time after the connection is closed, ``net_tcp: context->tcp == NULL`` will be logged.
