..
 # Copyright (c) 2022-2023, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

#########
Reproduce
#########

This section of the User Guide describes how to download, configure, build and
execute the different
:ref:`Use-Cases <introduction_use_cases_and_reference_software_stack_overview>`
provided with this Reference Stack.

************
Introduction
************

This Reference Stack uses the |kas build tool|_ to configure and customize the
different use cases via a set of configuration files.

The :repo:`yocto/kas` directory contains the kas configuration files to support
building and customization of the Reference Stack images. These kas
configuration files contain preset settings for the following use
cases:

  * ``baremetal.yml`` to prepare an image for the baremetal architecture.
  * ``virtualization.yml`` to prepare an image for the virtualization
    architecture.
  * ``systemready-ir-acs.yml`` to run |SystemReadyTM| IR |ACS|_ (Architectural
    Compliance Suite) tests.

These kas configuration files can be further used to build custom Reference
Stack images as described in the section
:ref:`Customization <user_guide_extend_customization>`.

.. note::
  All command examples on this page can be copied by clicking the copy button.
  Any console prompts at the start of each line, comments, or empty lines will
  be automatically excluded from the copied text.

.. _user_guide_reproduce_environment_setup:

****************************
Build Host Environment Setup
****************************


System Requirements
===================

    * x86_64 host to build and execute the RD-N2-Automotive FVP
    * Ubuntu 20.04 Linux distribution
    * At least 200GiB of free disk for the download and builds


Install Dependencies
====================

Please follow the Yocto Project documentation on
|how to install the essential packages|_ required for the build host.

Install the kas tool:

.. code-block:: console
  :substitutions:

  sudo -H pip3 install --upgrade kas==|kas version|

For more details on kas installation, see |kas Dependencies & installation|_.


.. _user_guide_reproduce_download:

********
Download
********

Download the ``rd-n2-automotive`` repository using Git, via:

.. code-block:: shell
  :substitutions:

  # Change the tag or branch to be fetched by replacing the value supplied to
  # the --branch parameter option

  mkdir -p ~/rd-n2-automotive
  cd ~/rd-n2-automotive
  git clone |rd-n2-automotive remote| --branch |rd_n2_automotive version|

.. _user_guide_reproduce_build:

*****
Build
*****

The provided kas configuration files can be used to build an image for
different system architectures, and to apply different sets of customizable
parameters. Therefore, the following build guidance is provided as a set of
alternatives to target each of the main supported use cases.

.. note::
  To build and run any image for the RD-N2 Automotive FVP the user has to
  accept its |EULA|_, which can be done by executing
  the following command in the build environment:

  .. code-block:: console

    export FVP_RD_N2_AUTOMOTIVE_EULA_ACCEPT=True

Baremetal Architecture
======================

To build a baremetal image:

  .. code-block:: console

    kas build --update rd-n2-automotive/yocto/kas/baremetal.yml

Validation tests can be run on the baremetal images.
See :ref:`reproduce_run-time_integration_tests` for more details on running
run-time validation tests.

Virtualization Architecture
===========================

To build a virtualization image:

  .. code-block:: console

    kas build --update rd-n2-automotive/yocto/kas/virtualization.yml

As with the baremetal guidance above, the Reference Stack virtualization
image can also run validation tests.
See :ref:`reproduce_run-time_integration_tests` for more details on
running run-time validation tests.

***
Run
***

This section describes how to run the Reference Stack on its FVP and connect to
the Primary Compute and Safety Island terminals to manually execute commands and
in this way try out its different functionalities. This can be done for both
the Baremetal and Virtualization Architectures.

.. note::
  FVPs, and Fast Models in general, are functionally accurate, meaning that they
  fully execute all instructions correctly, however they are not cycle accurate.
  The main goal of the Reference Stack is to prove functionality only, and
  should not be used for performance analysis.

The Reference Stack running on the Primary Compute can be logged into as
``root`` user without password in the Linux terminal.

Baremetal Architecture
======================

To start the FVP and connect to the Primary Compute terminal (running Linux):

  .. code-block:: console

    kas shell -c="../layers/meta-arm/scripts/runfvp --verbose --console" \
    rd-n2-automotive/yocto/kas/baremetal.yml

The user should wait for the system to boot and for the Linux prompt to appear.
For more information on how to interact with this terminal to reproduce the
Reference Stack's use-cases, please refer to the section
:ref:`Reproducing the Use-Cases <reproduce_reproducing_the_use-cases>`.

Virtualization Architecture
===========================

To start the FVP and connect to the Primary Compute terminal (running Linux):

  .. code-block:: console

    kas shell -c="../layers/meta-arm/scripts/runfvp --verbose --console" \
    rd-n2-automotive/yocto/kas/virtualization.yml

The user should wait for the system to boot and for the Linux prompt to appear.
On a virtualization image, this will access Dom0. Use the ``xl`` tool to log
in to the DomU:

  .. code-block:: console

    xl console domU

This command will provide a console on the DomU. To exit, one can enter
``Ctrl+]`` (to access the FVP telnet shell), followed by typing ``send esc``
into the telnet shell and pressing ``Enter``. See the |xl documentation|_ for
further details.

For more information on how to interact with this terminal to reproduce the
Reference Stack's use-cases, please refer to the section
:ref:`Reproducing the Use-Cases <reproduce_reproducing_the_use-cases>` (which
assumes the Primary Compute commands are being run from the DomU console).


.. _reproduce_reproducing_the_use-cases:

Reproducing the Use-Cases
=========================

These instructions can be ran on both Baremetal and Virtualization and assume
that the FVP has been launched as indicated above.

To reproduce the Heterogeneous Inter-processor Communication listed in the
:ref:`Use-Cases <introduction_use_cases_and_reference_software_stack_overview>`,
follow the steps listed below. Please refer to the |iperf manual page|_ and the
|zperf sample|_ for more information on `iperf` and `zperf` respectively.

.. note::

   When running the ``runfvp`` command, the Safety Island (SI) terminal is
   available via the Window entitled as **"FVP terminal_uart_si_cluster0"**.

1. Run the ``ping`` command from the Primary Compute (running Linux) to verify
   that it can communicate with the Safety Island (running Zephyr):

   .. code-block:: shell

      # On the Primary Compute terminal
      ping 192.168.1.2 -c 10

   The output should look like the following line, repeated 10 times:

   .. code-block:: shell

      64 bytes from 192.168.1.2: seq=0 ttl=64 time=0.151 ms

2. Run the ``net ping`` command from the Safety Island (running Zephyr) to
   verify that it can communicate with the Primary Compute (running Linux):

   .. code-block:: shell

      # On the Safety Island terminal
      net ping 192.168.1.1 -c 10

   The output should look like the following line, repeated 10 times:

   .. code-block:: shell

      28 bytes from 192.168.1.1 to 192.168.1.2: icmp_seq=0 ttl=64 time=0 ms

3. Stress test the communication between Linux as a UDP client (using
   ``iperf`` command) and Zephyr as a UDP server (using ``zperf`` command):

   .. code-block:: shell

      # On the Safety Island terminal
      zperf udp download

   A message similar to the following should appear:

   .. code-block:: shell

      UDP server started on port 5001

   .. code-block:: shell

      # On the Primary Compute terminal
      # iperf -u -c <dest ip> <duration> <bandwidth> <buffer_size>
      iperf -u -c 192.168.1.2 -t 5 -b 100K -l 1438

   No packets should be out of order and no packets should be lost. No error or
   warning should be displayed. The output should look similar to the following:

   .. code-block:: shell

      # On the Primary Compute terminal
      ------------------------------------------------------------
      Client connecting to 192.168.1.2, UDP port 5001
      Sending 1438 byte datagrams
      UDP buffer size:  208 KByte (default)
      ------------------------------------------------------------
      [  3] local 192.168.1.1 port 38733 connected with 192.168.1.2 port 5001
      [ ID] Interval       Transfer     Bandwidth
      [  3]  0.0- 5.2 sec  63.2 KBytes   100 Kbits/sec
      [  3] Sent 45 datagrams
      [  3] Server Report:
      [  3]  0.0- 5.2 sec  61.8 KBytes  97.7 Kbits/sec   4.654 ms    0/   44 (0%)

   .. code-block:: shell

      # On the Safety Island terminal
      New session started.
      End of session!
      duration:     5.18 s
      received packets: 44
      nb packets lost: 0
      nb packets outorder: 0
      jitter:    4.65 ms
      rate:    95 Kbps

4. Stress test the communication between Linux as a TCP client and Zephyr as a
   TCP server:

   .. code-block:: shell

      # On the Safety Island terminal
      zperf tcp download

   A message similar to the following should appear:

   .. code-block:: shell

      TCP server started on port 5001

   .. code-block:: shell

      # On the Primary Compute terminal
      # iperf -u -c <dest ip> <duration>
      iperf -c 192.168.1.2 -t 5

   No error or warning should be displayed. The output should look similar to
   the following:

   .. code-block:: shell

      # On the Primary Compute terminal
      ------------------------------------------------------------
      Client connecting to 192.168.1.2, TCP port 5001
      TCP window size: 85.0 KByte (default)
      ------------------------------------------------------------
      [  3] local 192.168.1.1 port 52622 connected with 192.168.1.2 port 5001
      [  753.544822] hrtimer: interrupt took 2020 ns
      [ ID] Interval       Transfer     Bandwidth
      [  3]  0.0- 5.0 sec  13.6 MBytes  22.8 Mbits/sec

   .. code-block:: shell

      # On the Safety Island terminal
      New TCP session started
      TCP session ended
      Duration:      5.05 s
      rate:          21.58 Mbps
      [00:12:38.490,000] <err> net_tcp: context->tcp == NULL
      [00:12:38.490,000] <err> net_tcp: context->tcp == NULL

   Please notice that the last error is a known limitation listed in
   :ref:`changelog_knownissues`.

5. Stress test the communication between Linux as a UDP server and Zephyr as a
   UDP client:

   .. code-block:: shell

      # On the Primary Compute terminal
      iperf -u -s -P 1

   .. code-block:: shell

      # On the Safety Island terminal
      # zperf udp upload <dest ip> <dest port> <duration> <packet size> <bandwidth>
      zperf udp upload 192.168.1.1 5001 5 1k 100K

   No packets should be out of order and no packets should be lost. No error or
   warning should be displayed. The output should look similar to the following:

   .. code-block:: shell

      # On the Primary Compute terminal
      ------------------------------------------------------------
      Server listening on UDP port 5001
      Receiving 1470 byte datagrams
      UDP buffer size:  208 KByte (default)
      ------------------------------------------------------------
      [  3] local 192.168.1.1 port 5001 connected with 192.168.1.2 port 44883
      [ ID] Interval       Transfer     Bandwidth        Jitter   Lost/Total Datagrams
      [  3]  0.0- 5.1 sec  64.0 KBytes   103 Kbits/sec   0.001 ms    0/   64 (0%)

   .. code-block:: shell

      # On the Safety Island terminal
      Remote port is 5001
      Connecting to 192.168.1.1
      Duration:    5.00 s
      Packet size: 1024 bytes
      Rate:        100 kbps
      Starting...
      Rate:   100 Kbps
      Packet duration 80 ms
      nb_packets=14 delay=70000	adjust=0
      nb_packets=27 delay=70000	adjust=0
      nb_packets=40 delay=70000	adjust=0
      nb_packets=53 delay=70000	adjust=0
      -
      Upload completed!
      Statistics:    server (client)
      Duration:      5.08 s (5.09 s)
      Num packets:   64  (64)
      Num packets out order: 0
      Num packets lost: 0
      Jitter:           1 us
      Rate: 100 Kbps (100 Kbps)

6. Stress test the communication between Linux as a TCP server and Zephyr as a
   TCP client:

   .. code-block:: shell

      # On the Primary Compute terminal
      iperf -s -P 1

   .. code-block:: shell

      # On the Safety Island terminal
      # zperf tcp upload <dest ip> <dest port> <duration> <packet size>
      zperf tcp upload 192.168.1.1 5001 5 1k

   The number of errors that appear on screen should be zero. The output should
   look similar to the following:

   .. code-block:: shell

      # On the Primary Compute terminal
      ------------------------------------------------------------
      Server listening on TCP port 5001
      TCP window size:  128 KByte (default)
      ------------------------------------------------------------
      [  4] local 192.168.1.1 port 5001 connected with 192.168.1.2 port 40732
      [ ID] Interval       Transfer     Bandwidth
      [  4]  0.0- 5.0 sec  11.0 MBytes  18.5 Mbits/sec

   .. code-block:: shell

      # On the Safety Island terminal
      Remote port is 5001
      Connecting to 192.168.1.1
      Duration:      5.00 s
      Packet size:   1024 bytes
      Rate:    10 kbps
      Starting...
      New session started
      -
      Upload completed!
      Duration:      5.00 s
      Num packets:   11281
      Num errors: 0 (retry or fail)
      Rate:    17.62 Mbps

7.  To finish the FVP emulation, you need to close the telnet session from the
    Primary Compute terminal:

    a. Escape to telnet console with ``ctrl+]``.

    b. Run ``quit`` to close the session and terminate the FVP execution.

    c. You can now close the terminal where the Safety Island telnet session was
       running.

.. _reproduce_run-time_integration_tests:

**********
Validation
**********

The following validation tests can be performed on the Reference Stack:

  * System Integration Tests:

    * Baremetal Architecture Stack:

      .. code-block:: console

        TESTIMAGE_AUTO=1 kas build rd-n2-automotive/yocto/kas/baremetal.yml

      The previous test takes around 30 minutes to complete.

      A similar output should be printed out:

      .. code-block:: console

        NOTE: Executing Tasks
        2022-12-07 09:05:58 - INFO     - Creating terminal default on terminal_ns_uart_ap
        2022-12-07 09:05:58 - INFO     - Creating terminal tf-a on terminal_s_uart_ap
        2022-12-07 09:05:58 - INFO     - Creating terminal scp on terminal_uart_scp
        2022-12-07 09:05:58 - INFO     - Creating terminal safety_island_c0 on terminal_uart_si_cluster0
        2022-12-07 09:05:59 - INFO     - Creating terminal rss on terminal_uart_rss
        2022-12-07 09:05:59 - INFO     - default: Waiting for login prompt
        2022-12-07 09:21:10 - INFO     - Starting ptest-runner
        2022-12-07 09:30:37 - INFO     - RESULTS:
        2022-12-07 09:30:37 - INFO     - RESULTS - linuxboot.LinuxBootTest.test_linux_boot: PASSED (339.17s)
        2022-12-07 09:30:37 - INFO     - RESULTS - linuxlogin.LinuxLoginTest.test_linux_login: PASSED (4.98s)
        2022-12-07 09:30:37 - INFO     - RESULTS - ptest_ssh.PtestRunnerTest.test_ptestrunner: PASSED (138.44s)
        2022-12-07 09:30:37 - INFO     - RESULTS - reset.ResetTest.test_linux_login: PASSED (421.70s)
        2022-12-07 09:30:37 - INFO     - RESULTS - rss.RssTest.test_normal_boot: PASSED (0.00s)
        2022-12-07 09:30:37 - INFO     - RESULTS - safety_island.SafetyIslandTest.test_hipc: PASSED (553.03s)
        2022-12-07 09:30:37 - INFO     - RESULTS - safety_island.SafetyIslandTest.test_ping: PASSED (13.89s)
        2022-12-07 09:30:37 - INFO     - RESULTS - scp_firmware.ScpTest.test_normal_boot: PASSED (0.00s)
        2022-12-07 09:30:37 - INFO     - RESULTS - trusted_firmware_a.TrustedFirmwareTest.test_normal_boot: PASSED (0.00s)
        2022-12-07 09:30:37 - INFO     - SUMMARY:
        2022-12-07 09:30:37 - INFO     - baremetal-image () - Ran 9 tests in 1471.225s
        2022-12-07 09:30:37 - INFO     - baremetal-image - OK - All required tests passed (successes=9, skipped=0, failures=0, errors=0)
        2022-12-07 09:30:37 - INFO     - NOTE: recipe baremetal-image-1.0-r0: task do_testimage: Succeeded

    * Virtualization Architecture Stack:

      .. code-block:: console

        TESTIMAGE_AUTO=1 kas build rd-n2-automotive/yocto/kas/virtualization.yml

      The previous test takes around 30 minutes to complete.

      A similar output should be printed out:

      .. code-block:: console

        NOTE: Executing Tasks
        2022-12-07 09:08:06 - INFO     - Creating terminal default on terminal_ns_uart_ap
        2022-12-07 09:08:07 - INFO     - Creating terminal tf-a on terminal_s_uart_ap
        2022-12-07 09:08:07 - INFO     - Creating terminal scp on terminal_uart_scp
        2022-12-07 09:08:07 - INFO     - Creating terminal safety_island_c0 on terminal_uart_si_cluster0
        2022-12-07 09:08:08 - INFO     - Creating terminal rss on terminal_uart_rss
        2022-12-07 09:08:08 - INFO     - default: Waiting for login prompt
        2022-12-07 09:18:03 - INFO     - ptest-runner started
        2022-12-07 09:25:32 - INFO     - Starting ptest-runner
        2022-12-07 09:33:28 - INFO     - RESULTS:
        2022-12-07 09:33:28 - INFO     - RESULTS - linuxboot.LinuxBootTest.test_linux_boot: PASSED (589.59s)
        2022-12-07 09:33:28 - INFO     - RESULTS - linuxlogin.LinuxLoginTest.test_linux_login: PASSED (1.33s)
        2022-12-07 09:33:28 - INFO     - RESULTS - ptest_domu.DomUPtestRunnerTest.test_ptestrunner: PASSED (69.20s)
        2022-12-07 09:33:28 - INFO     - RESULTS - ptest_ssh.PtestRunnerTest.test_ptestrunner: PASSED (465.74s)
        2022-12-07 09:33:28 - INFO     - RESULTS - rss.RssTest.test_normal_boot: PASSED (0.00s)
        2022-12-07 09:33:28 - INFO     - RESULTS - safety_island.SafetyIslandTest.test_hipc: PASSED (356.24s)
        2022-12-07 09:33:28 - INFO     - RESULTS - safety_island.SafetyIslandTest.test_ping: PASSED (27.59s)
        2022-12-07 09:33:28 - INFO     - RESULTS - scp_firmware.ScpTest.test_normal_boot: PASSED (0.00s)
        2022-12-07 09:33:28 - INFO     - RESULTS - trusted_firmware_a.TrustedFirmwareTest.test_normal_boot: PASSED (0.00s)
        2022-12-07 09:33:28 - INFO     - SUMMARY:
        2022-12-07 09:33:28 - INFO     - virtualization-image () - Ran 9 tests in 1509.685s
        2022-12-07 09:33:28 - INFO     - virtualization-image - OK - All required tests passed (successes=9, skipped=0, failures=0, errors=0)
        2022-12-07 09:33:28 - INFO     - NOTE: recipe virtualization-image-1.0-r0: task do_testimage: Succeeded

  * |SystemReadyTM| IR |ACS|_ (Architectural Compliance Suite) Tests:

    .. code-block:: console

      kas build rd-n2-automotive/yocto/kas/systemready-ir-acs.yml

    This last command builds the |SystemReadyTM| IR firmware and runs the
    |SystemReadyTM| IR |ACS|_ tests on it. The full battery of tests takes
    around 14 hours to complete.

    A similar output should be printed out:

    .. code-block:: console

      (...)
      2022-10-16 04:56:20 - INFO     - Test Group (SetMode_Func): PASSED
      2022-10-16 04:56:21 - INFO     - Test Group (SetMode_Func): PASSED
      2022-10-16 04:57:41 - INFO     - Test Group (TestString_Func): PASSED
      2022-10-16 04:57:42 - INFO     - Test Group (TestString_Func): PASSED
      2022-10-16 04:59:21 - INFO     - Resetting
      2022-10-16 04:59:23 - INFO     - Creating terminal default on terminal_ns_uart_ap
      2022-10-16 04:59:26 - INFO     - Creating terminal tf-a on terminal_s_uart_ap
      2022-10-16 04:59:26 - INFO     - Creating terminal scp on terminal_uart_scp
      2022-10-16 04:59:26 - INFO     - Creating terminal safety_island_c0 on terminal_uart_si_cluster0
      2022-10-16 04:59:27 - INFO     - Creating terminal rss on terminal_uart_rss
      2022-10-16 05:08:06 - INFO     - Test Group (volatile Variable Reset Test): PASSED
      2022-10-16 05:09:35 - INFO     - Test Group (Runtime Services Test): PASSED
      2022-10-16 05:10:54 - INFO     - Test Group (TestEfiSpecVerLvl): PASSED
      2022-10-16 05:12:13 - INFO     - Test Group (BootExcLevel): PASSED
      2022-10-16 05:13:31 - INFO     - Test Group (MCastIpToMac_Conf): PASSED
      2022-10-16 05:14:50 - INFO     - Test Group (MCastIpToMac_Func): PASSED
      2022-10-16 05:16:49 - INFO     - Resetting
      2022-10-16 05:16:51 - INFO     - Creating terminal default on terminal_ns_uart_ap
      2022-10-16 05:16:53 - INFO     - Creating terminal tf-a on terminal_s_uart_ap
      2022-10-16 05:16:53 - INFO     - Creating terminal scp on terminal_uart_scp
      2022-10-16 05:16:53 - INFO     - Creating terminal safety_island_c0 on terminal_uart_si_cluster0
      2022-10-16 05:16:55 - INFO     - Creating terminal rss on terminal_uart_rss
      2022-10-16 05:26:55 - INFO     - Bitbake still alive (no events for 600s). Active tasks:
      2022-10-16 05:26:55 - INFO     - /work/kas_build_dir/../kas_work_dir/layers/rd-n2-automotive/yocto/meta-rd-n2-automotive/recipes-test/systemready-acs/systemready-ir-acs.bb:do_testimage
      2022-10-16 05:27:02 - INFO     - Test Group (virtio_blk virtio1): vda
      2022-10-16 05:27:19 - INFO     - Linux tests complete
      2022-10-16 05:27:25 - INFO     - RESULTS:
      2022-10-16 05:27:25 - INFO     - RESULTS - systemready_ir_acs.SystemReadyACSTest.test_acs: PASSED (37896.58s)
      2022-10-16 05:27:25 - INFO     - SUMMARY:
      2022-10-16 05:27:25 - INFO     - systemready-ir-acs () - Ran 1 test in 37896.583s
      2022-10-16 05:27:25 - INFO     - systemready-ir-acs - OK - All required tests passed (successes=1, skipped=0, failures=0, errors=0)
      2022-10-16 05:27:28 - INFO     - ACS test suite results are consistent with baseline.
      2022-10-16 05:27:28 - INFO     - NOTE: recipe systemready-ir-acs-1.0-r0: task do_testimage: Succeeded
      2022-10-16 05:27:29 - INFO     -
      2022-10-16 05:27:29 - INFO     - Summary: There was 1 WARNING message.

  Please refer to :ref:`validation` for an explanation on how
  the validation tests are set up and how they work in the Reference Stack.
  Please refer to :ref:`boot_process_systemready-status` for an explanation of
  the stack's |SystemReadyTM| support.
