..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

##########
User Guide
##########

.. toctree::
   :maxdepth: 1
   :caption: Contents

   reproduce

Describes how to reproduce a Reference Stack image, and how to configure, build
run and validate the supported set of architecture features and use-cases.

.. toctree::
   :maxdepth: 1

   extend

Describes how to extend the Reference Stack to build for a custom architecture,
and how to customize the build for different architectural features and
use-cases.

.. toctree::
   :maxdepth: 1

   borrow

Provides an overview of the key components and the applied changes for this
project so that the user can determine how to isolate and build them
independently and import them into their existing project.
