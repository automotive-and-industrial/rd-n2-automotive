..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

######
Extend
######

This section of the User Guide describes how to customize the build for
different architectural features and
:ref:`Use-Cases <introduction_use_cases_and_reference_software_stack_overview>`.

.. _user_guide_extend_customization:

*************
Customization
*************

The Reference Stack defines a set of standard customizable environment
variables that can be used to configure different architectural features.

Virtualization
==============

The Reference Stack defines a set of standard customizable environment
variables for configuring the domains included on a virtualization image. The
following list shows the variables and their default values (where ``MB``
refers to Megabytes):

  .. code-block:: yaml
    :substitutions:

    |virtualization customization yaml|

To customize these standard variables, set their value in the environment for
the kas build. For example, to build a virtualization image allocating a
non-default value of two CPUs and ``512 MB`` of memory for its DomU, run:

  .. code-block:: console

    DOMU_MEMORY_SIZE=512 DOMU_VCPUS=2 kas build --update rd-n2-automotive/yocto/kas/virtualization.yml
