..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

###############
Solution Design
###############

.. toctree::
   :titlesonly:
   :maxdepth: 2
   :caption: Contents

   boot_process
   hipc
   components
   integration
   validation
