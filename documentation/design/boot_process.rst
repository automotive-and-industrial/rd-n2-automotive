..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

############
Boot Process
############
.. _boot_process_rss-oriented_boot_flow:

.. _design_boot_process_rss-oriented_boot_flow:

**********************
RSS-oriented Boot Flow
**********************

The :ref:`design_components_rss` is the root of the trust chain. It is the
first booting element when the system is powered up.

The RSS, implemented in Trusted Firmware-M (TF-M), has 3 boot stages. The
images for each stage are stored in different media:

* RSS BL1 (corresponds to TF-M BL11) is in ROM
* RSS BL2 (TF-M BL12) is in OTP
* RSS BL3 (TF-M BL2) is in NVM flash

The NVM flash contains not only RSS BL3, but also other images that are booted
by the RSS. The images currently included in the flash are:

* RSS BL3
* RSS Runtime
* Application Processor BL1 (AP BL1)
* Safety Island Cluster 0 (SI CL0)
* SCP ROM Firmware (SCP ROMFW)

:ref:`design_components_scp-firmware` has been extended to additionally release
the Safety Island from reset and to synchronize power control with the loading
of images by the RSS.

The following diagram depicts the layout of the NVM flash.

|

.. image:: ../images/rss_flash_layout.svg
   :align: center

|

The following diagram illustrates the boot flow that originates from the RSS.

|

.. image:: ../images/rss_oriented_boot_flow.svg
   :align: center

|

Major steps of the boot flow:

1. RSS BL1 begins executing in place from ROM when the system is powered up. It:

   * Copies RSS BL2 from OTP to SRAM
   * Verifies RSS BL2 against the hash stored in OTP
   * Jumps to RSS BL2, if the hash verification has succeeded

2. RSS BL2:

   * Copies RSS BL3 image from flash into SRAM
   * Verifies RSS BL3 image using asymmetric cryptography
   * Jumps to RSS BL3, if the image was successfully verified

3. RSS BL3:

   * Copies SCP ROMFW from flash to SCP SRAM and verifies the image
   * Resets the SCP
   * Copies SI CL0 from flash to SI SRAM and verifies the image
   * Notifies the SCP to reset the SI
   * Copies AP BL1 from flash to AP SRAM and verifies the image
   * Notifies the SCP to power on the AP

4. SCP ROM Firmware starts SCP RAM Firmware in SRAM

SCP RAM Firmware is not loaded by the RSS. Please refer to
:ref:`changelog_limitations` for more information.

*************************
Primary Compute Boot Flow
*************************

The Primary Compute is the Application Processor in the Automotive Reference
Design. The purpose of its firmware is to provide a |SystemReadyTM| IR aligned
interface to Linux. |SystemReadyTM| IR compatible systems are required to
follow the |Device Tree specification|_, so the :ref:`design_components_u-boot`
bootloader is used in the non-secure world, which provides the UEFI
implementation and exposes the :ref:`design_components_device-tree` to Linux.

:ref:`design_components_trusted-firmware-a` provides the initial, secure-world
firmware, which consists of BL1, BL2 and BL31. BL33 is provided by
U-Boot.

The Primary Compute boot flow follows on from the
:ref:`design_boot_process_rss-oriented_boot_flow`:

5. AP BL1:

   * Copies AP BL2 from flash to SRAM
   * Jumps to AP BL2

6. AP BL2:

   * Copies AP BL31 and BL33 from flash to SRAM
   * Jumps to AP BL31

7. AP BL31 starts AP BL33
8. AP BL33 starts the Linux operating system

******************
|SystemReadyTM| IR
******************

|Arm SystemReady|_ is a compliance certification program based on a set of
hardware and firmware standards that enable interoperability with generic
off-the-shelf operating systems and hypervisors. These standards include the
|Base System Architecture (BSA)|_ and |Base Boot Requirements (BBR)|_
specifications, and market-specific supplements.

In this way, the |Arm SystemReady program|_ provides a formal set of compute
platform definitions to cover a range of systems from the cloud to IoT and edge,
helping software 'just work' seamlessly across an ecosystem of Arm-based
hardware.

|SystemReadyTM| is divided into a set of bands with a combination of specs
available to suit the different devices and markets. |SystemReadyTM| IR is one
of these bands.

|Arm SystemReady IR|_ certified platforms implement a minimum set of hardware
and firmware features that an operating system can depend on to deploy the
operating system image. Hence, |SystemReadyTM| IR ensures the deployment and
maintenance of standard firmware interfaces and targets both custom (Yocto,
OpenWRT, buildroot) and pre-built (Debian, Fedora, SUSE).

At a high level, the IR band requires that:

 * Firmware implements a subset of UEFI as defined in Embedded Base Boot
   Requirements (EBBR)
 * Firmware by default provides a device tree suitable for booting mainline
   Linux
 * Firmware can be updated using UEFI UpdateCapsule()
 * At least two Linux distros must be able to boot and install using the UEFI
   boot flow

Compliant systems must conform to the:

 * |Base System Architecture (BSA)|_ specification
 * |Embedded Base Boot Requirements (EBBR)|_
 * EBBR recipe of the Arm |Base Boot Requirements (BBR)|_ specification
 * |Device Tree specification|_

|SystemReadyTM| IR Objective
============================

This Reference Stack aims to be aligned with |SystemReadyTM| IR version 1.0, but
does not aim to be |SystemReadyTM| IR certified, meaning that neither formal
compliance testing nor validation are performed.

.. _boot_process_systemready-status:

Current Status
==============

This Reference Stack has the testing capability to check for |SystemReadyTM|
alignment. Please refer to :ref:`reproduce_run-time_integration_tests` to
see how to run the |SystemReadyTM| IR |ACS|_ tests in this Reference Stack.

The |SystemReadyTM| IR ACS tests of the Reference Stack use a set of baseline
files to detect any changes in regards to the current status of the
|SystemReadyTM| IR alignment of the system. These files can be found under
:repo:`yocto/meta-rd-n2-automotive/lib/acs` and describe the current status of
the Reference Stack. A high-level summary of the current non-alignments is
described in :ref:`boot_process_systemready-non_alignments`. Please refer
to the baseline files for more detailed information on each individual set of
tests.

.. _boot_process_systemready-non_alignments:

Identified non-alignments
=========================

The Reference Stack is currently known to have the following non-alignments:

 * Reference stack

      1. RD-N2 software implementation does not currently support capsule
         updates, so the UpdateCapsule() method is currently being invoked with
         invalid parameters (CapsuleCount - 0).
      2. U-Boot uses the 'removable storage' method to boot the EFI payload and
         the EFI boot manager is not configured/used.

 * U-Boot

      1. Known limitations of EFI implementation which are excluded in the
         |EBBR specification- UEFI runtime services|_.
      2. Known limitations of EFI implementation which are noted as 'Explicit
         justification in a future revision of EBBR is pending' by
         |edk2-test-parser|_.
      3. The UpdateCapsule() method does not currently support certain
         invocations with invalid parameters.

 * Model - FVP

      1. Platform-specific limitations, which are noted as excluded in the
         |EBBR specification - Required Platform Specific Elements|_.
      2. AES, SHA1 and SHA2 instructions are marked as unavailable in the FVP
         ID_AA64ISAR0_EL1.

 * Test environment

      1. No text input is available in the test environment for Simple Text
         Input Ex protocol.

 * BSA tests

      1. Tests are not compatible with certain devices in the RD-N2-Automotive
         model.

|SystemReadyTM| IR Tests
========================

Please refer to :ref:`validation_systemready_ir_tests` for more information on
the |SystemReadyTM| IR test structure.
