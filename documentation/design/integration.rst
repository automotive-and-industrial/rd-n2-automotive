..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

###########
Integration
###########

The Arm |Neoverse| N2 Automotive Reference Software Stack uses the Yocto
Project build framework to build, integrate and validate the
:ref:`Use-Cases <introduction_use_cases_and_reference_software_stack_overview>`
.

The Yocto Project version used by the Reference Stack is |yocto version|.

*********************************
meta-rd-n2-automotive Yocto Layer
*********************************

The ``meta-rd-n2-automotive`` BSP layer implements the ``fvp-rd-n2-automotive``
bitbake ``MACHINE`` definition to enable the Reference Stack to run on the Arm
|Neoverse| N2 Automotive Reference Design FVP (FVP_RD_N2_Automotive). It also
contains a set of bitbake bbclasses, recipes and libraries to build, integrate
and validate both the **Baremetal** and **Virtualization** Reference Stack
Architectures as described in :ref:`Reference Stack Overview 
<introduction_use_cases_and_reference_software_stack_overview>`.

The layer source code can be found at :repo:`yocto/meta-rd-n2-automotive`

Yocto Build Configuration
=========================

A set of ``yaml`` configuration files (found at :repo:`yocto/kas`) for the
|kas build tool|_ is provided to support bitbake layer fetching, project
configuration and executing the build and validation. The description of the
kas configuration files can be found in the
:ref:`Reproduce <user_guide/reproduce:Reproduce>` section of the User Guide.


Yocto Layers Dependency
=======================

The following diagram illustrates the layers which are integrated as part of
the Reference Stack.

|

.. image:: ../images/rd_n2_automotive_yocto_layers_dependency_diagram.svg
   :align: center

|

Note that the ``meta-virtualization`` layer is only required when building the
**Virtualization Architecture**.

The layer dependency sources and their revisions for the ``rd-n2-automotive``
repository (|rd-n2-automotive repository|) |layer dependency statement| are:

  .. code-block:: yaml
    :substitutions:

    URL: |meta-arm repository|
    layers: meta-arm, meta-arm-toolchain
    branch: |meta-arm branch|
    revision: |meta-arm revision|

    URL: |poky repository|
    layers: meta, meta-poky
    branch: |poky branch|
    revision: |poky revision|

    URL: |meta-openembedded repository|
    layers: meta-filesystems, meta-networking, meta-oe, meta-python
    branch: |meta-openembedded branch|
    revision: |meta-openembedded revision|

    URL: |meta-zephyr repository|
    layers: meta-zephyr-core
    branch: |meta-zephyr branch|
    revision: |meta-zephyr revision|

    URL: |meta-virtualization repository|
    layers: meta-virtualization
    branch: |meta-virtualization branch|
    revision: |meta-virtualization revision|

