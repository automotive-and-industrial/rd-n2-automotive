# Copyright (c) 2022-2023, Arm Limited.
#
# SPDX-License-Identifier: MIT

# This file centralizes the variables and links used throughout the
# documentation. The dictionaries are converted to a single string that is used
# as the rst_prolog (see the Sphinx Configuration documentation at
# [inclusivity-exception]
# https://www.sphinx-doc.org/en/master/usage/configuration.html for more info).

# There are two types of key-value substitutions:
#     1. simple string replacements
#     2. replacement with a rendered hyperlink, where the key defines what the
#        rendered hyperlink text will be

# Prepend the key with "link:" to identify it as a Sphinx target name for use
# as a hyperlink. The "link:" prefix is dropped from the substitution name.
#
# For example:
#   "link:This URL": "www.arm.com"
#   "company name": "arm"
# Can be used as:
#   The |company name| website can be found at |This URL|_.
#
# Note the "_" which renders the substitution as a hyperlink is only possible
# because the variable is defined as a link, to be resolved as a Sphinx target.

yocto_version = "langdale"
kas_version = "3.1"
xen_version = "4.16"
zephyr_version = "3.2.0"
trusted_firmware_m_version = "a1e8602d5ec2acad923b24eb436dc0f663eea649"
# inclusivity-exception
trusted_firmware_m_base_version = "master branch post 1.6.0"
scp_firmware_version = "RD-INFRA-2022.08.18"
scp_firmware_base_version = "2.10"
trusted_firmware_a_version = "RD-INFRA-2022.08.18"
trusted_firmware_a_base_version = "2.7"
uboot_version = "2022.07"
linux_version = "5.19"
linux_version_patch = "17"
yocto_doc_version = yocto_version + "/"
rd_n2_automotive_version = "v1.0"
fvp_version = "11.20.20"

general_links = {
  "link:GitLab Issues": "https://gitlab.arm.com/automotive-and-industrial/rd-n2-automotive/-/issues",
  "link:kas build tool": f"https://kas.readthedocs.io/en/{kas_version}/userguide.html",
  "link:how to install the essential packages": f"https://docs.yoctoproject.org/{yocto_doc_version}singleindex.html#required-packages-for-the-build-host",
  "link:kas Dependencies & installation": f"https://kas.readthedocs.io/en/{kas_version}/userguide.html#dependencies-installation",
  "link:xl documentation": f"https://xenbits.xen.org/docs/{xen_version}-testing/man/xl.1.html",
  "link:EULA": f"https://developer.arm.com/downloads/-/arm-ecosystem-fvps/eula",
  "link:ACS": f"https://developer.arm.com/Architectures/Architectural%20Compliance%20Suite",
  "link:Arm Confidential Compute Architecture": "https://www.arm.com/architecture/security-features/arm-confidential-compute-architecture",
  "link:SCP-firmware": "https://github.com/ARM-software/SCP-firmware",
  "link:Fast Models FVP Reference Guide": "https://developer.arm.com/documentation/100966/latest",
  "link:Arm Ecosystem Fixed Virtual Platform": "https://developer.arm.com/downloads/-/arm-ecosystem-fvps",
  "link:Fast Models FVP Reference Guide": "https://developer.arm.com/documentation/100966/latest",
  "link:Arm Neoverse N2 Automotive Reference Design Technical Overview": "https://developer.arm.com/documentation/107916/0100",
  "link:Yocto Project Quick Start": f"https://docs.yoctoproject.org/{yocto_doc_version}brief-yoctoprojectqs/index.html",
  "link:Runtime Security Subsystem (RSS)": "https://tf-m-user-guide.trustedfirmware.org/platform/arm/rss/readme.html",
  "link:System Control Processor (SCP)": "https://developer.arm.com/tools-and-software/open-source-software/%20firmware/scp-firmware",
  "link:Trusted Firmware-A (TF-A)": "https://trustedfirmware-a.readthedocs.io",
  "link:Trusted Firmware-M (TF-M)": "https://tf-m-user-guide.trustedfirmware.org",
  "link:U-boot": "https://u-boot.readthedocs.io",
  "link:Zephyr": "https://docs.zephyrproject.org",
  "link:Xen Hypervisor": "https://xenproject.org/help/documentation",
  "link:Arm SystemReady": "https://www.arm.com/architecture/system-architectures/systemready-certification-program",
  "link:Arm SystemReady program": "https://www.arm.com/architecture/system-architectures/systemready-certification-program",
  "link:Arm SystemReady IR": "https://www.arm.com/architecture/system-architectures/systemready-certification-program/ir",
  "link:Base System Architecture (BSA)": "https://developer.arm.com/documentation/den0094/latest",
  "link:Embedded Base Boot Requirements (EBBR)": "https://developer.arm.com/architectures/platform-design/embedded-systems",
  "link:Base Boot Requirements (BBR)": "https://developer.arm.com/documentation/den0044/latest",
  "link:Device Tree specification": "https://www.devicetree.org/",
  "link:EBBR specification- UEFI runtime services": "https://arm-software.github.io/ebbr/index.html#uefi-runtime-services",
  "link:edk2-test-parser": "https://gitlab.arm.com/systemready/edk2-test-parser/-/blob/ir1/EBBR.yaml",
  "link:testimage": f"https://docs.yoctoproject.org/{yocto_doc_version}ref-manual/classes.html#testimage-bbclass",
  "link:EBBR specification - Required Platform Specific Elements": "https://arm-software.github.io/ebbr/index.html#required-platform-specific-elements",
  "link:Writing New Tests": f"https://docs.yoctoproject.org/{yocto_doc_version}dev-manual/common-tasks.html#writing-new-tests",
  "link:Bash Automated Test System": "https://github.com/bats-core/bats-core",
  "link:Yocto Package Test": "https://wiki.yoctoproject.org/wiki/Ptest",
  "link:testimage.bbclass": f"https://docs.yoctoproject.org/{yocto_doc_version}ref-manual/classes.html#testimage-bbclass",
  "link:OEQA FVP": f"https://git.yoctoproject.org/meta-arm/tree/documentation/oeqa-fvp.md?h={yocto_version}",
  "link:Arm Memory Partitioning and Monitoring": "https://developer.arm.com/documentation/ddi0598/latest",
  "link:zperf sample": f"https://docs.zephyrproject.org/{zephyr_version}/connectivity/networking/api/zperf.html",
  "link:Xen repository": f"https://xenbits.xen.org/gitweb/?p=xen.git;a=tree;h=refs/heads/stable-4.16;h=stable-{xen_version}",
  "link:Zephyr repository": f"https://github.com/zephyrproject-rtos/zephyr/tree/v{zephyr_version}",
  "link:Trusted Firmware-M repository": f"https://git.trustedfirmware.org/TF-M/trusted-firmware-m.git/tree/?h={trusted_firmware_m_version}",
  "link:SCP-Firmware repository": f"https://gitlab.arm.com/arm-reference-solutions/scp-firmware/-/tree/{scp_firmware_version}",
  "link:Trusted Firmware-A repository": f"https://gitlab.arm.com/arm-reference-solutions/trusted-firmware-a/-/tree/{trusted_firmware_a_version}",
  "link:U-Boot repository": f"https://source.denx.de/u-boot/u-boot/-/tree/v{uboot_version}",
  "link:Linux repository": f"https://git.yoctoproject.org/linux-yocto/log/?h=v{linux_version}%2Fstandard%2Fbase",
  "link:iperf manual page": "https://linux.die.net/man/1/iperf",
  "link:FVP download": "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Automotive%20FVPs/FVP_RD_N2_Automotive_11.20_20_Linux64.tgz",
}

layer_definitions = {
  "rd-n2-automotive repository": "https://gitlab.arm.com/automotive-and-industrial/rd-n2-automotive",
  "rd-n2-automotive remote": "https://git.gitlab.arm.com/automotive-and-industrial/rd-n2-automotive.git",
  "meta-arm repository": "https://git.yoctoproject.org/git/meta-arm",
  "poky repository": "https://git.yoctoproject.org/git/poky",
  "meta-openembedded repository": "https://git.openembedded.org/meta-openembedded",
  "meta-virtualization repository": "https://git.yoctoproject.org/git/meta-virtualization",
  "meta-zephyr repository": "https://git.yoctoproject.org/git/meta-zephyr",
  "rd_n2_automotive version": f"{rd_n2_automotive_version}",
  "meta-arm branch": f"{yocto_version}",
  "poky branch": f"{yocto_version}",
  "meta-openembedded branch": f"{yocto_version}",
  "meta-virtualization branch": f"{yocto_version}",
  "meta-zephyr branch": f"{yocto_version}",
  "meta-arm revision": "025124814e8676e46d42ec5b07220283f1bdbcd0",
  "poky revision": "3ec705d3203766a9a437ef7c7837f820c0800ead",
  "meta-openembedded revision": "c354f92778c1d4bcd3680af7e0fb0d1414de2344",
  "meta-virtualization revision": "8857b36ebfec3d548755755b009adc491ef320ab",
  "meta-zephyr revision": "030c1e2d8960dd09b7de239d62092e812eaceb42",
  "layer dependency statement": f"``{rd_n2_automotive_version}`` release",
}

other_definitions = {
  "kas version": f"{kas_version}",
  "yocto version": f"``{yocto_version}``",
  "zephyr version": f"{zephyr_version}",
  "Xen version": f"{xen_version}",
  "Trusted Firmware-M version": f"{trusted_firmware_m_version}",
  "Trusted Firmware-M base version": f"{trusted_firmware_m_base_version}",
  "SCP-Firmware version": f"{scp_firmware_version}",
  "SCP-Firmware base version": f"{scp_firmware_base_version}",
  "Trusted Firmware-A version": f"{trusted_firmware_a_version}",
  "Trusted Firmware-A base version": f"{trusted_firmware_a_base_version}",
  "U-Boot version": f"{uboot_version}",
  "Linux version": f"{linux_version}.{linux_version_patch}",
  "FVP_RD_N2_Automotive version": f"{fvp_version}",
  "virtualization customization yaml":
      """
      DOMU_VCPUS: "4"                                    # Number of CPUs for DomU
      DOMU_MEMORY_SIZE: "1024"                           # Memory for DomU (in MB)
      """,
  "inclusivity-exception": "\ ",
  "edk2-test-parser recipe path": "yocto/meta-rd-n2-automotive/recipes-test/systemready-acs/edk2-test-parser.bb",
  "systemready-scripts recipe path": "yocto/meta-rd-n2-automotive/recipes-test/systemready-acs/systemready-scripts.bb",
  "systemready-ir-acs recipe path": "yocto/meta-rd-n2-automotive/recipes-test/systemready-acs/systemready-ir-acs.bb",
  "Neoverse": "Neoverse\ :sup:`TM`",
  "Arm": "Arm\ :sup:`®`",
  "Cortex": "Arm\ :sup:`®` Cortex\ :sup:`®`",
  "SystemReadyTM": "Arm SystemReady\ :sup:`TM`",
}


def generate_link(key, link):

    definition = f".. _{key}: {link}"
    key_mapping = f".. |{key}| replace:: {key}"
    return f"{definition}\n{key_mapping}"


def generate_replacement(key, value):

    replacement = f".. |{key}| replace:: {value}"
    return f"{replacement}"


def generate_rst_prolog():

    rst_prolog = ""

    for variables_group in [general_links,
                            layer_definitions,
                            other_definitions]:

        for key, value in variables_group.items():
            if key.startswith("link:"):
                rst_prolog += generate_link(key.split("link:")[1], value) + "\n"
            else:
                rst_prolog += generate_replacement(key, value) + "\n"

    return rst_prolog
